import matplotlib.pyplot as plt
import geopandas as gpd
import seaborn as sns
import pandas as pd
import numpy as np
import rasterio as rio
from sklearn.metrics import f1_score

import glob

from luisanet import sampling

dict_luisa_class = {
1111:'lvl4 - 1111 - High density urban fabric',
1121:'lvl4 - 1121 - Medium density urban fabric',
1122:'lvl4 - 1122 - Low density urban fabric',
1123:'lvl4 - 1123 - Isolated or very low density urban fabric',
1130:'lvl4 - 1130 - Urban vegetation',
1210:'lvl4 - 1210 - Industrial or commercial units',
1221:'lvl4 - 1221 - Road and rail networks and associated land',
1222:'lvl4 - 1222 - Major stations',
1230:'lvl4 - 1230 - Port areas',
1241:'lvl4 - 1241 - Airport areas',
1242:'lvl4 - 1242 - Airport terminals',
1310:'lvl4 - 1310 - Mineral extraction sites',
1320:'lvl4 - 1320 - Dump sites',
1330:'lvl4 - 1330 - Construction sites',
1410:'lvl4 - 1410 - Green urban areas',
1421:'lvl4 - 1421 - Sport and leisure green',
1422:'lvl4 - 1422 - Sport and leisure built-up',
2110:'lvl4 - 2110 - Non irrigated arable land',
2120:'lvl4 - 2120 - Permanently irrigated land',
2130:'lvl4 - 2130 - Rice fields',
2210:'lvl4 - 2210 - Vineyards',
2220:'lvl4 - 2220 - Fruit trees and berry plantations',
2230:'lvl4 - 2230 - Olive groves',
2310:'lvl4 - 2310 - Pastures',
2410:'lvl4 - 2410 - Annual crops associated with permanent crops',
2420:'lvl4 - 2420 - Complex cultivation patterns',
2430:'lvl4 - 2430 - Land principally occupied by agriculture',
2440:'lvl4 - 2440 - Agro-forestry areas',
3110:'lvl4 - 3110 - Broad-leaved forest',
3120:'lvl4 - 3120 - Coniferous forest',
3130:'lvl4 - 3130 - Mixed forest',
3210:'lvl4 - 3210 - Natural grassland',
3220:'lvl4 - 3220 - Moors and heathland',
3230:'lvl4 - 3230 - Sclerophyllous vegetation',
3240:'lvl4 - 3240 - Transitional woodland shrub',
3310:'lvl4 - 3310 - Beaches, dunes and sand plains',
3320:'lvl4 - 3320 - Bare rock',
3330:'lvl4 - 3330 - Sparsely vegetated areas',
3340:'lvl4 - 3340 - Burnt areas',
3350:'lvl4 - 3350 - Glaciers and perpetual snow',
4000:'lvl4 - 4000 - Wetlands',
5110:'lvl4 - 5110 - Water courses',
5120:'lvl4 - 5120 - Water bodies',
5210:'lvl4 - 5210 - Coastal lagoons',
5220:'lvl4 - 5220 - Estuaries',
5230:'lvl4 - 5230 - Sea and ocean',
}


list_dict_class_colors = [{
        1: '#dc0000',
    2: '#e6cc4d',
    3: '#388a00',
    4: '#7a7aff',
    5: '#00b4f0',
    },
    {
    11: '#dc0000',
    12: '#646464',
    13: '#a64d00',
    14: '#55ff00',
    21: '#ffffa8',
    22: '#f2a64d',
    23: '#e6e64d',
    24: '#e6cc4d',
    31: '#388a00',
    32: '#89cd66',
    33: '#cccccc',
    40: '#7a7aff',
    51: '#00b4f0',
    52: '#e6f2ff',
}]
    

def get_shortened_integer(number_to_shorten):
    """ Takes integer and returns a formatted string """

    if number_to_shorten == 0:
        return 0
    trailing_zeros = np.floor(np.log10(abs(number_to_shorten)))
    if trailing_zeros < 3:
        # Ignore everything below 1000
        return str(int(trailing_zeros))
    elif 3 <= trailing_zeros <= 5:
        # Truncate thousands, e.g. 1.3k
        return str(int(round(number_to_shorten/(10**3), 0))) + 'k'
    elif 6 <= trailing_zeros <= 8:
        # Truncate millions like 3.2M
        return str(int(round(number_to_shorten/(10**6), 0))) + 'M'
    else:
        raise ValueError('Values larger or equal to a billion not supported')


def plot_confusion_matrices(predictions_dir, dir_name, out_dir, lvl,loss):

    file_list = glob.glob(f'{predictions_dir}{dir_name}/{loss}/*_class.tif')
    file_list.sort()

    input_url = 'http://92.111.153.194:9000/eumap/lcv/lcv_landcover_luisa_jrc_30m_0..0cm_2018_eumap_epsg3035_v0.1.tif'


    if lvl == 1:
        divider = 1000
        dict_lvl = list_dict_class_colors[lvl-1]
        colors = [color for value,color in list_dict_class_colors[lvl-1].items()]
        levels = [value - 0.5 for value, color in list_dict_class_colors[lvl-1].items()]
    elif lvl == 2:
        divider = 100
        dict_lvl = list_dict_class_colors[lvl-1]
        colors = [color for value,color in list_dict_class_colors[lvl-1].items()]
        levels = [value - 0.5 for value, color in list_dict_class_colors[lvl-1].items()]

    for tile_url in file_list:

        str_tile_nr =  tile_url.split('/')[-1].split('.')[0].split('_')[0]

        tile_file = rio.open(tile_url)
        tile_data = tile_file.read()


        sample_window = sampling.WindowSampler(input_urls = [input_url],
                                        window=tile_file.bounds)

        lvl_data = np.round(sample_window.input_data[0,:,:]/divider,0).astype(int)

        y_true = pd.Series(lvl_data.flatten() , name='Actual')
        y_pred = pd.Series(tile_data[0,:,:].flatten(), name='Predicted')
        df_confusion = pd.crosstab(y_true, y_pred)

        list_all_classes = np.unique([value for value,text in dict_lvl.items()])

        df_confusion_full = pd.DataFrame(np.zeros([len(list_all_classes),len(list_all_classes)]), columns = list_all_classes, index = list_all_classes, dtype = int)

        for row in list_all_classes:
            for column in list_all_classes:
                try:
                    df_confusion_full.loc[column,row] = df_confusion.loc[column,row]
                except:
                    pass

        pdf_conf = pd.DataFrame(df_confusion_full)

        df_confusion_str = df_confusion_full.applymap(get_shortened_integer)

        f1_score_result = np.round(f1_score(list(y_true), list(y_pred), average='micro'),3)

        ## plotting



        plt.figure(figsize = (10,13))
        ax1 = plt.subplot(211)
        sns.heatmap(df_confusion_full, fmt='',  annot = df_confusion_str, linewidths=.5, square= True, ax= ax1);
        plt.xlabel('predicted')
        plt.ylabel('true')
        plt.title(f'{str_tile_nr} \n f-1 score: {str(f1_score_result)}' );

        ax2 = plt.subplot(223)
        ax2.contourf(np.rot90(lvl_data.T), levels=levels, colors=colors)
        ax2.set_title('True')
        ax2.axis('off')
        ax2.set_aspect("equal")

        ax3 = plt.subplot(224)
        ax3.contourf(np.rot90(tile_data[0,:,:].T), levels=levels, colors=colors)
        ax3.set_title('Predicted')
        ax3.axis('off')
        ax3.set_aspect("equal")

        plt.savefig(f'{out_dir}{dir_name}_{str_tile_nr}_confusion_matrix.png')
        

if __name__ == "__main__":
        
    dir_name = 'luisanet_lvl1_uk_landsat_hydra'
    
    predictions_dir = '/mnt/hydra/luisanet/predictions/'
    out_dir = '../aux-data/accuracy_metrics/'

    plot_confusion_matrices(predictions_dir, dir_name, out_dir, lvl = 1, loss=1.005)