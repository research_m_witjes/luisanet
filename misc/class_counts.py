from geopy.geocoders import Nominatim
import matplotlib.pyplot as plt
import geopandas as gpd
import seaborn as sns
import pandas as pd
import numpy as np

from luisanet import sampling

dict_luisa_class = {
1111:'lvl4 - 1111 - High density urban fabric',
1121:'lvl4 - 1121 - Medium density urban fabric',
1122:'lvl4 - 1122 - Low density urban fabric',
1123:'lvl4 - 1123 - Isolated or very low density urban fabric',
1130:'lvl4 - 1130 - Urban vegetation',
1210:'lvl4 - 1210 - Industrial or commercial units',
1221:'lvl4 - 1221 - Road and rail networks and associated land',
1222:'lvl4 - 1222 - Major stations',
1230:'lvl4 - 1230 - Port areas',
1241:'lvl4 - 1241 - Airport areas',
1242:'lvl4 - 1242 - Airport terminals',
1310:'lvl4 - 1310 - Mineral extraction sites',
1320:'lvl4 - 1320 - Dump sites',
1330:'lvl4 - 1330 - Construction sites',
1410:'lvl4 - 1410 - Green urban areas',
1421:'lvl4 - 1421 - Sport and leisure green',
1422:'lvl4 - 1422 - Sport and leisure built-up',
2110:'lvl4 - 2110 - Non irrigated arable land',
2120:'lvl4 - 2120 - Permanently irrigated land',
2130:'lvl4 - 2130 - Rice fields',
2210:'lvl4 - 2210 - Vineyards',
2220:'lvl4 - 2220 - Fruit trees and berry plantations',
2230:'lvl4 - 2230 - Olive groves',
2310:'lvl4 - 2310 - Pastures',
2410:'lvl4 - 2410 - Annual crops associated with permanent crops',
2420:'lvl4 - 2420 - Complex cultivation patterns',
2430:'lvl4 - 2430 - Land principally occupied by agriculture',
2440:'lvl4 - 2440 - Agro-forestry areas',
3110:'lvl4 - 3110 - Broad-leaved forest',
3120:'lvl4 - 3120 - Coniferous forest',
3130:'lvl4 - 3130 - Mixed forest',
3210:'lvl4 - 3210 - Natural grassland',
3220:'lvl4 - 3220 - Moors and heathland',
3230:'lvl4 - 3230 - Sclerophyllous vegetation',
3240:'lvl4 - 3240 - Transitional woodland shrub',
3310:'lvl4 - 3310 - Beaches, dunes and sand plains',
3320:'lvl4 - 3320 - Bare rock',
3330:'lvl4 - 3330 - Sparsely vegetated areas',
3340:'lvl4 - 3340 - Burnt areas',
3350:'lvl4 - 3350 - Glaciers and perpetual snow',
4000:'lvl4 - 4000 - Wetlands',
5110:'lvl4 - 5110 - Water courses',
5120:'lvl4 - 5120 - Water bodies',
5210:'lvl4 - 5210 - Coastal lagoons',
5220:'lvl4 - 5220 - Estuaries',
5230:'lvl4 - 5230 - Sea and ocean',
}


def get_shortened_integer(number_to_shorten):
    """ Takes integer and returns a formatted string """

    if number_to_shorten == 0:
        return 0
    trailing_zeros = np.floor(np.log10(abs(number_to_shorten)))
    if trailing_zeros < 3:
        # Ignore everything below 1000
        return str(int(trailing_zeros))
    elif 3 <= trailing_zeros <= 5:
        # Truncate thousands, e.g. 1.3k
        return str(int(round(number_to_shorten/(10**3), 0))) + 'k'
    elif 6 <= trailing_zeros <= 8:
        # Truncate millions like 3.2M
        return str(int(round(number_to_shorten/(10**6), 0))) + 'M'
    else:
        raise ValueError('Values larger or equal to a billion not supported')


def count_classes(gdf_bboxes, input_url):

    print('Counting classes in bboxes')

    list_classes_int = [x for x,y in dict_luisa_class.items()]
    list_classes_str = [y for x,y in dict_luisa_class.items()]

    gdf_bboxes_wgs84 = gdf_bboxes.to_crs('epsg:4326')

    nr_bboxes = gdf_bboxes.shape[0]
    nr_classes = len(list_classes_int)
    arr_counts = np.array(np.zeros([nr_bboxes, nr_classes]).astype(int))

    for i in range(nr_bboxes):

        window = gdf_bboxes.geometry[i].bounds

        sample_window = sampling.WindowSampler(input_urls = [input_url],
                                    window=window)

        list_classes = list(sample_window.input_data[0,:,:].flatten())

        list_unique_classes = np.unique(list_classes)

        counts = {value: list_classes.count(value) for value in list_unique_classes}

        for entry in counts.items():

            class_index = np.where(list_classes_int == entry[0])[0][0]

            arr_counts[i,class_index] = int(entry[1])


    print('Getting bbox location names')
    geolocator = Nominatim(user_agent="myapp")

    str_list_places = []
    i = 0

    for geometry in gdf_bboxes_wgs84.geometry:
        x,y = geometry.centroid.xy
        location = geolocator.reverse(f'{y[0]}, {x[0]}')

        str_list_places.append(f'ID: {i}, {location.address.split(",")[-1]},{location.address.split(",")[-5]}')

        i+=1

    pdf_counts = pd.DataFrame(arr_counts, columns=list_classes_str, index=str_list_places)



    gdf_bboxes = gdf_bboxes[['layer','geometry']].join(pdf_counts.reset_index(drop=True)).drop('layer', axis = 1)

      

    gdf_bboxes.to_file('../aux-data/planet_bboxes_epsg3035_aligned_counts_v0.1.gpkg', driver="GPKG")
    pdf_counts.to_csv('../aux-data/class_counts.csv')

    return pdf_counts


if __name__ == "__main__":

    from matplotlib.colors import LogNorm

    gdf_bboxes = gpd.read_file('../aux-data/planet_bboxes_epsg3035_aligned.gpkg')

    input_url = 'http://92.111.153.194:9000/eumap/lcv/lcv_landcover_luisa_jrc_30m_0..0cm_2018_eumap_epsg3035_v0.1.tif'

    pdf_counts = count_classes(gdf_bboxes, input_url)


    pdf_counts_str = pdf_counts.applymap(get_shortened_integer)


    fig = plt.figure(figsize=(20,10)) 
        
    ax = plt.subplot(111) 
    fig.add_subplot(ax)
        
    fig.set_facecolor("white")

    sns.heatmap(pdf_counts, fmt='',  annot = pdf_counts_str, linewidths=.5, norm=LogNorm())


    print('saving count plot to ../aux-data/class_count_per_tile.png')
    plt.savefig('../aux-data/class_count_per_tile.png')
