import pandas as pd
import geopandas as gpd
import numpy as np

import seaborn as sns
from matplotlib.colors import LogNorm
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split



dict_luisa_class = {
1111:'lvl4 - 1111 - High density urban fabric',
1121:'lvl4 - 1121 - Medium density urban fabric',
1122:'lvl4 - 1122 - Low density urban fabric',
1123:'lvl4 - 1123 - Isolated or very low density urban fabric',
1130:'lvl4 - 1130 - Urban vegetation',
1210:'lvl4 - 1210 - Industrial or commercial units',
1221:'lvl4 - 1221 - Road and rail networks and associated land',
1222:'lvl4 - 1222 - Major stations',
1230:'lvl4 - 1230 - Port areas',
1241:'lvl4 - 1241 - Airport areas',
1242:'lvl4 - 1242 - Airport terminals',
1310:'lvl4 - 1310 - Mineral extraction sites',
1320:'lvl4 - 1320 - Dump sites',
1330:'lvl4 - 1330 - Construction sites',
1410:'lvl4 - 1410 - Green urban areas',
1421:'lvl4 - 1421 - Sport and leisure green',
1422:'lvl4 - 1422 - Sport and leisure built-up',
2110:'lvl4 - 2110 - Non irrigated arable land',
2120:'lvl4 - 2120 - Permanently irrigated land',
2130:'lvl4 - 2130 - Rice fields',
2210:'lvl4 - 2210 - Vineyards',
2220:'lvl4 - 2220 - Fruit trees and berry plantations',
2230:'lvl4 - 2230 - Olive groves',
2310:'lvl4 - 2310 - Pastures',
2410:'lvl4 - 2410 - Annual crops associated with permanent crops',
2420:'lvl4 - 2420 - Complex cultivation patterns',
2430:'lvl4 - 2430 - Land principally occupied by agriculture',
2440:'lvl4 - 2440 - Agro-forestry areas',
3110:'lvl4 - 3110 - Broad-leaved forest',
3120:'lvl4 - 3120 - Coniferous forest',
3130:'lvl4 - 3130 - Mixed forest',
3210:'lvl4 - 3210 - Natural grassland',
3220:'lvl4 - 3220 - Moors and heathland',
3230:'lvl4 - 3230 - Sclerophyllous vegetation',
3240:'lvl4 - 3240 - Transitional woodland shrub',
3310:'lvl4 - 3310 - Beaches, dunes and sand plains',
3320:'lvl4 - 3320 - Bare rock',
3330:'lvl4 - 3330 - Sparsely vegetated areas',
3340:'lvl4 - 3340 - Burnt areas',
3350:'lvl4 - 3350 - Glaciers and perpetual snow',
4000:'lvl4 - 4000 - Wetlands',
5110:'lvl4 - 5110 - Water courses',
5120:'lvl4 - 5120 - Water bodies',
5210:'lvl4 - 5210 - Coastal lagoons',
5220:'lvl4 - 5220 - Estuaries',
5230:'lvl4 - 5230 - Sea and ocean',
}



dict_lvl2_LUISA = {
11:'lvl2 - 11 - Urban fabric',
12:'lvl2 - 12 - Industrial, commercial and transport units',
13:'lvl2 - 13 - Mine, dump and construction sites',
14:'lvl2 - 14 - Artificial, non-agricultural vegetated areas',
21:'lvl2 - 21 - Arable land',
22:'lvl2 - 22 - Permanent crops',
23:'lvl2 - 23 - Pastures',
24:'lvl2 - 24 - Heterogeneous agricultural areas',
31:'lvl2 - 31 - Forest and seminatural areas',
32:'lvl2 - 32 - Shrub and/or herbaceous vegetation associations',
33:'lvl2 - 33 - Open spaces with little or no vegetation',
40:'lvl2 - 40 - Wetlands',
51:'lvl2 - 51 - Inland waters',
52:'lvl2 - 52 - marine waters'
}

inv_dict_luisa_class = dict([(name,code) for code,name in dict_luisa_class.items()])

def get_shortened_integer(number_to_shorten):
    """ Takes integer and returns a formatted string """

    if number_to_shorten == 0:
        return 0
    trailing_zeros = np.floor(np.log10(abs(number_to_shorten)))
    if trailing_zeros < 3:
        # Ignore everything below 1000
        return str(int(trailing_zeros))
    elif 3 <= trailing_zeros <= 5:
        # Truncate thousands, e.g. 1.3k
        return str(int(round(number_to_shorten/(10**3), 0))) + 'k'
    elif 6 <= trailing_zeros <= 8:
        # Truncate millions like 3.2M
        return str(int(round(number_to_shorten/(10**6), 0))) + 'M'
    else:
        raise ValueError('Values larger or equal to a billion not supported')



def convert_lvl4_lvl2(pdf_class_counts):

    columns = [item for item, value in dict_lvl2_LUISA.items()]
    pdf_lvl2 = pd.DataFrame(data = np.zeros([12, len(columns)]), columns=columns)

    return pdf_lvl2

def train_test_split_planet_tiles(pdf_lvl2, fn_gpkg, seed = 99):



    gdf_bboxes = gpd.read_file(fn_gpkg)

    gdf_bboxes['train'] = 0



    pdf_class_counts.rename(columns=inv_dict_luisa_class, inplace=True)

    for code,name in dict_lvl2_LUISA.items():

        col_select = [str(title)[:2] == str(code) for title in pdf_class_counts.columns]


        #find sum of columns specified 
        pdf_lvl2[code] = pdf_class_counts[pdf_class_counts.columns[col_select]].sum(axis=1)


    gdf_bboxes = gdf_bboxes.join(pdf_lvl2.rename(columns=dict_lvl2_LUISA).reset_index(drop=True))


    col_select = [str(title)[:2] == str(code) for title in pdf_class_counts.columns]

    pdf_class_counts.columns[col_select]
   



    pdf_lvl2_thr = pdf_lvl2 > 1e3

    # consider classes with 2 or more tiles that have the class
    list_selected_classes = pdf_lvl2_thr.columns[pdf_lvl2_thr.sum(axis = 0).values >= 2]

    if seed == 99:
        train, test = train_test_split(range(pdf_lvl2.shape[0]), test_size = .3)
    else:
        train, test = train_test_split(range(pdf_lvl2.shape[0]), test_size = .3, random_state = seed)

    print(train, test)

    classes_check = False

    if seed == 99:
        pass
    else:
        np.random.seed(seed)

    while not classes_check:

        pdf_test = pdf_lvl2_thr.loc[test, list_selected_classes]
        pdf_train = pdf_lvl2_thr.loc[train, list_selected_classes]

        if np.all(pdf_train.sum(axis=0) > 0) & np.all(pdf_test.sum(axis=0) > 0):
            print('All classes in both groups')
            classes_check = True
        else:
            # print('switching tile')
            switch_classes_test = [luisa_class for luisa_class,count in pdf_test.sum(axis=0).items() if count < 1]
            switch_classes_train = [luisa_class for luisa_class,count in pdf_train.sum(axis=0).items() if count < 1]

            if len(switch_classes_test) > 0:
                # print('switch tile with ', switch_classes_train, ' from train to test' )

                switch_tile_train = np.random.choice(pdf_train[pdf_train.loc[:,[switch_classes_test[0]]].values].T.columns, 1, replace=False)[0]

                switch_tile_test = np.random.choice(pdf_test.T.columns, 1, replace=False)[0] 

                train.remove(switch_tile_train)
                train.append(switch_tile_test)

                test.remove(switch_tile_test)
                test.append(switch_tile_train)


            else:

                switch_tile_test = np.random.choice(pdf_test[pdf_test.loc[:,[switch_classes_train[0]]].values].T.columns, 1, replace=False)[0]

                switch_tile_train = np.random.choice(pdf_train.T.columns, 1, replace=False)[0] 

                train.remove(switch_tile_train)
                train.append(switch_tile_test)

                test.remove(switch_tile_test)
                test.append(switch_tile_train)
                pass
                

        print(train, test)


    gdf_bboxes.loc[train,'train'] = 1

    print('Write train test split to: ../aux-data/planet_bboxes_epsg3035_train_test.gpkg')

    return gdf_bboxes

if __name__ == "__main__":

    fn_class_count = '../aux-data/class_counts.csv'
    fn_gpkg = '../aux-data/planet_bboxes_epsg3035_aligned_counts_v0.1.gpkg'

    # read data
    pdf_class_counts = pd.read_csv(fn_class_count)

    # convert lvl4 to lvl2 LUISA classes in class count
    pdf_lvl2 = convert_lvl4_lvl2(pdf_class_counts)

    # generate train test split
    gdf_bboxes = train_test_split_planet_tiles(pdf_lvl2, fn_gpkg, seed = 42)

    gdf_bboxes.to_file('../aux-data/planet_bboxes_epsg3035_aligned_train_test_v0.1.gpkg', driver="GPKG")

    # plot class pixel counts per tile
    pdf_lvl2_str = pdf_lvl2.applymap(get_shortened_integer)
    pdf_lvl2_luisa_name = pdf_lvl2.rename(columns=dict_lvl2_LUISA)

    plt.figure(figsize=(15,10))
    plt.title('LUISA class lvl2 occurence counts \n only classes with >1k pixels in tile');
    sns.heatmap(pdf_lvl2_luisa_name.T[pdf_lvl2_luisa_name.T > 1000], fmt='',  annot = pdf_lvl2_str.T, linewidths=.5, norm=LogNorm(), square=True);
    print('saving image to ../aux-data/LUISA_class_lvl2_occ_count.png')
    plt.savefig('../aux-data/LUISA_class_lvl2_occ_count.png')


