# Luisanet

## Project description

## Install

### With dependencies

#### 1. Clone the repo

`git clone https://gitlab.com/research_m_witjes/luisanet.git`

#### 2. Make a conda env

`conda env create -f environment.yml`

#### 3. Install pytorch

By following [the instructions](https://pytorch.org/get-started/locally).

#### 4. Install luisanet

`python setup.py install`

### Without dependencies

`python -m pip install git+https://gitlab.com/research_m_witjes/luisanet.git`

## Usage

`python -m luisanet.bin.00_testing`

`python -m luisanet.bin.01_preprocess`

`python -m luisanet.bin.02_train_multi`

`python -m luisanet.bin.03_train`

`python -m luisanet.bin.04_predict`
