import joblib
import os
import geopandas as gpd
from glob import glob
import numpy as np

dir_root = 'C:\\Users\\Martijn\\Documents\\werk\\luisanet\\'
region = "nl" # odse, pilot
years_luisa = ['2012','2018']
overwrite = True

# Extract LUCAS points from larger dataset; skip if exists
dir_points = dir_root + "data\\points\\"
path_points = dir_points + f"points_{region}.gpkg"
assert os.path.exists(path_points), f"ERROR: {path_points} does not exist"
path_lucas = dir_points + f"points_{region}-lucas.gpkg"
path_lucas_2012 = dir_points + f"points_{region}-lucas-2012"
if not os.path.exists(path_lucas) and not overwrite:
    pts = joblib.load(path_points) if path_points.endswith(".lz4") else gpd.read_file(path_points)
    lucas = pts.loc[pts.lucas]
    lucas.to_file(path_lucas,driver='GPKG')
    print(f"Writing {len(lucas)} LUCAS points to \n\t {path_lucas}")
    pts = None
else:
    lucas = gpd.read_file(path_lucas)


# Sample the minimum number of available points among 2012 and 2018 to write two equal datasets
lucas['year'] = lucas.survey_date.str[:4]
n = np.min([len(lucas.loc[lucas.year == year]) for year in years_luisa])

for year in years_luisa:
    path_lucas_year = path_lucas[:-5] + f"-{year}.gpkg"
    lucas_year = lucas.loc[lucas.year == year].sample(n); lucas_year.to_file(path_lucas_year,driver='GPKG')
    print(f"Writing {len(lucas_year)} LUCAS points for {year} to \n\t {path_lucas_year}")




