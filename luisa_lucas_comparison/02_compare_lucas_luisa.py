import sklearn
import geopandas as gpd
import pandas as pd
import numpy as np
import os
import os.path as osp

import legend


dir_root = 'C:\\Users\\Martijn\\Documents\\werk\\luisanet\\'
region = "nl" # odse, pilot
years_luisa = ['2012','2018']
overwrite = True


for year in years_luisa:
    dir_points = osp.join(dir_root, "data", "points")
    path_points = osp.join(dir_points, f"points_{region}-lucas-{year}-overlaid.gpkg")
    pts = gpd.read_file(path_points)
    for i in range(1,5):
        pts[f'luisa_{i}'] = pts.rvalue_1.astype(str).str[:i]
        print(pts[f"luisa_{i}"].value_counts(),f'{year}')

pts['lucas_1'] = pts.lc1.str[:1]
pts['lucas_1_lbl'] = pts.lucas_1.replace(legend.lucas_1)
pts['lucas_3_lbl'] = pts.lc1.replace(legend.lucas_3)
pts['luisa_1_lbl'] = pts.luisa_1.replace(legend.luisa_1)
pts['luisa_4_lbl'] = pts.luisa_4.replace(legend.luisa_4)

dir_analysis = osp.join(dir_root,"analysis")
os.makedirs(dir_analysis,exist_ok=True)    

for c in pts.columns:
    print(c)

ct = pd.crosstab(pts.lucas_3_lbl,pts.luisa_4_lbl)
path_crosstab = osp.join(dir_analysis,'crosstab_lucas-3_luisa_4.csv')
print(ct)
ct.to_csv(path_crosstab)
