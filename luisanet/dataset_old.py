import torch
import joblib
import geopandas as gpd
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from glob import glob
import os
import os.path as osp

class SegmentationDataset(torch.utils.data.Dataset):
    def __init__(
        self,
        path_y=None,
        dir_x=None,
        path_tiles=None,
        tile_idxs = [],
        size_tile = 250,
        size_input = 128,
        ):

        # Get the tiling system and if specified a subset
        self.tile_idxs = tile_idxs
        self.tiles = gpd.read_file(path_tiles)
        if len(tile_idxs) > 0:
            self.tiles = self.tiles.iloc[tile_idxs]
        
        self.path_y = path_y
        self.dir_x = dir_x
        self.size_input = size_input
        self.size_tile = size_tile

        assert osp.exists(path_y), f'Y not found at {path_y}'



    def __len__(self):
        return ((self.size_tile-self.size_input)**2) * len(self.tiles)

    def __getitem__(self,idx):
        return 0



class LandsatDataset(torch.utils.data.Dataset):
    def __init__(
        self, 
        path_points, dir_blocks,
        y,
        transform=None,
        landsat=True,bands=None,
        window=None,
        elevation=False):

        points = joblib.load(path_points).to_crs(epsg=28992)
        vc = points[y].value_counts()
        # min_count = np.min(vc)
        self.points = points
        # self.points = points.groupby('clc_1', group_keys=False).apply(lambda x: x.sample(min_count,replace=True))
        self.dir_blocks = dir_blocks
        self.transform = transform
        self.y = y
        self.landsat = landsat
        self.bands = bands
        self.elevation = elevation
        labels = sorted(self.points[y].unique())
        self.label_dict = dict(zip(labels,range(len(labels))))
        print(self.label_dict)
        self.window =  window
        
    def __len__(self):
        return len(self.points)
    
    def __getitem__(self,idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        
        point = self.points.iloc[idx]
        y = self.label_dict[point[self.y]]
        y = np.array([y])
        
        block = []

        if self.landsat:
            path_block = f"{self.dir_blocks}/{point.name}/data_30m.joblib"
            landsat_data = joblib.load(path_block).astype(np.float32)
            if self.bands is not None:
                landsat_data = landsat_data[self.bands,:,:]
            block.append(landsat_data)

        if self.elevation:
            path_block = f"{self.dir_blocks}/{point.name}/data_5m.joblib"
            elevation_data = joblib.load(path_block).astype(np.float32)
            block.append(elevation_data)
        
        if self.window is not None:
            for i in range(len(block)):
                center = (block[i].shape[1]//2)
                wmin = center-self.window
                wmax = center+self.window+1
                block[i] = block[i][:,wmin:wmax,wmin:wmax]

        if self.landsat + self.elevation == 1:
            block = block[0]
        
        sample = [block, y]
        
        if self.transform:
            sample = self.transform(sample)
        
        return sample
    
    def inspect(self,idx):
        
        fig, ax = plt.subplots(1,7,figsize=[70,10])
        sample = self.__getitem__(idx)
        plt.tight_layout()
        for i in range(7):
            if bands is None:
                band = [4,16,28,40,52,64,76][i]
            else:
                band = bands[:7][i]
            ax[i].imshow(sample[0][band], cmap=['Reds','Greens','Blues','magma','magma','magma','gist_heat'][i])
            ax[i].set_title(['Red','Green','Blue','NIR','SWIR1','SWIR2','Thermal'][i],size=40); ax[i].axis('off')


class MultiDataset(torch.utils.data.Dataset):
    def __init__(
        self, 
        path_points, dir_blocks,
        y,
        transform=None,
        landsat=False,bands=None,
        point_columns_x=None,
        window=None,
        elevation=False):

        points = joblib.load(path_points).to_crs(epsg=28992)
        vc = points[y].value_counts()
        # min_count = np.min(vc)
        self.points = points
        # self.points = points.groupby('clc_1', group_keys=False).apply(lambda x: x.sample(min_count,replace=True))
        self.dir_blocks = dir_blocks
        self.transform = transform
        self.y = y
        self.landsat = landsat
        self.bands = bands
        self.point_columns_x = point_columns_x
        self.elevation = elevation
        labels = sorted(self.points[y].unique())
        self.label_dict = dict(zip(labels,range(len(labels))))
        print(self.label_dict)
        self.window =  window
        
    def __len__(self):
        return len(self.points)
    
    def __getitem__(self,idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        
        point = self.points.iloc[idx]
        y = self.label_dict[point[self.y]]
        y = np.array([y])
        
        block = []

        if self.landsat:
            path_block = f"{self.dir_blocks}/{point.name}/data_30m.joblib"
            landsat_data = joblib.load(path_block).astype(np.float32)
            if self.bands is not None:
                landsat_data = landsat_data[self.bands,:,:]
            block.append(landsat_data)

        if self.elevation:
            path_block = f"{self.dir_blocks}/{point.name}/data_5m.joblib"
            elevation_data = joblib.load(path_block).astype(np.float32)
            block.append(elevation_data)
        
        if self.window is not None:
            for i in range(len(block)):
                center = (block[i].shape[1]//2)
                wmin = center-self.window
                wmax = center+self.window+1
                block[i] = block[i][:,wmin:wmax,wmin:wmax]

        if self.point_columns_x is not None:
            values = point[self.point_columns_x].values.astype(np.float32)
            block.append(values)

        if len(block) == 1:
            block = block[0]
        

        
        sample = [block, y]
        
        
        if self.transform:
            sample = self.transform(sample)

        
        return sample