import torch
from typing import Iterable
import numpy as np

from . import legends
from . import sampling
from . import util
from . import config

class LuisaDataset(torch.utils.data.Dataset):
    def __init__(
        self,
        input_urls,
        windows=None,
        window_dim=128,
        fill_value=0,
        legend_key = legends.luisa4_to_idx,
        aggregator = legends.luisa4_to_luisa2,
        transforms = 2,
        stride=1,
        bands=None
    ):
        self.legend_key = legend_key
        self.sampler = sampling.MultiWindowSampler(
            input_urls,
            windows=windows,
        )
        self.aggregator = aggregator
        self.n_bands = len(input_urls) if bands is None else bands
        self.n_windows = len(self.sampler.subsamplers)
        self.window_dim = window_dim
        self.fill_value = fill_value
        self.n_augs = transforms
        self.stride = stride
        self._h = self.sampler.target_data.shape[0] // stride - window_dim // stride - 1
        self._w = self.sampler.target_data.shape[1] // stride - window_dim // stride - 1
        self.n_samples_per_window = self._h * self._w
        self.n_samples = self.n_samples_per_window * self.n_windows
        self.n_augs_per_window = self.n_samples_per_window * self.n_augs

    def _expand_index(self, idx):
        idx_win = idx // self.n_augs_per_window
        _idx_in_win_aug = idx % self.n_augs_per_window
        idx_aug = _idx_in_win_aug // self.n_samples_per_window
        _idx_in_win = _idx_in_win_aug % self.n_samples_per_window
        i = _idx_in_win // self._w * self.stride
        j = _idx_in_win % self._w * self.stride
        return idx_win, idx_aug, i, j

    def __len__(self):
        return self.n_samples * self.n_augs

    def _get_sample_by_index(self, idx):
        i_win, i_aug, i, j = self._expand_index(idx)
        x, y = self.sampler.subsamplers[i_win]._get_sample(
            i, j,
            self.window_dim,
            fill_value=self.fill_value,
        )
        return sampling.augment_by_index(i_aug, x, y)

    def __getitem__(self, idx):
        if torch.is_tensor(idx): # not sure what this does ~ luka
            idx = idx.tolist()

        if not isinstance(idx, Iterable):
            idx = [idx]

        output = [*map(
            np.stack,
            zip(*map(self._get_sample_by_index, idx,)))]

        output[0] = np.squeeze(output[0]).astype(np.float32)

        output[1][output[1] == None] = config.luisa_ocean
        output[1] =legends.reclass(output[1],self.aggregator)
        
        output[1] = legends.reclass(output[1],self.legend_key)
        
        return output

    def inspect(self, idx):
        import matplotlib.pyplot as plt

        cmaps = ['Blues', 'Greens', 'Reds', 'magma', 'magma', 'magma', 'gist_heat']

        fig, ax = plt.subplots(
            1,
            self.n_bands,
            figsize=[
                10*self.n_bands,
                10,
            ]
        )

        sample = self.__getitem__(idx)
        plt.tight_layout()
        for i in range(self.n_bands):
            try:
                cmap = cmaps[i]
            except IndexError:
                cmap = 'magma'
            ax[i].imshow(sample[0][0][i], cmap=cmap)
