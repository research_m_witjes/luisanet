import dataclasses as dc
from typing import Union, Iterable
from pathlib import Path

from . import legends

# Define main directories with source, intermediate, and output data
project_name = 'luisanet'
dir_storage_slow = f'/mnt/gaia/tmp/{project_name}/data'
dir_storage_s3 = f"/mnt/gaia/minio/data/"
dir_storage_fast = f'/mnt/fastboy/tmp/{project_name}/data'
dir_preprocessing = f'/mnt/atlas/{project_name}/data'
dir_deep_learning = f'/mnt/hydra/{project_name}'


# Paths and directories of input datasets
path_points_source = f'{dir_storage_slow}/points/points_44_preprocessed_tiled.joblib'
path_nl = f'{dir_storage_slow}/netherlands.gpkg'
dir_landsat = f"{dir_preprocessing}/landsat_ard"
dir_ahn = f"{dir_preprocessing}/ahn"
path_luisa = f'{dir_storage_s3}/eumap/lcv/lcv_landcover_luisa_jrc_30m_0..0cm_2018_eumap_epsg3035_v0.1.tif'

# Paths of preprocessed datasets
path_points_preprocessed = f'{dir_storage_fast}/points/training_points_nl.joblib'
dir_blocks = f'{dir_storage_fast}/blocks'

# Paths of grids and tiling systems
path_tiles_eu30km = f'{dir_storage_slow}/eu_tiling system_30km.gpkg'


luisa_nodata = -9223372036854775808
luisa_ocean = 5230
input_urls_landsat = [
    'http://92.111.153.194:9000/eumap/lcv/lcv_blue_landsat.glad.ard_p50_30m_0..0cm_2018.06.25..2018.09.12_eumap_epsg3035_v1.1.tif',
    'http://92.111.153.194:9000/eumap/lcv/lcv_green_landsat.glad.ard_p50_30m_0..0cm_2018.06.25..2018.09.12_eumap_epsg3035_v1.1.tif',
    'http://92.111.153.194:9000/eumap/lcv/lcv_red_landsat.glad.ard_p50_30m_0..0cm_2018.06.25..2018.09.12_eumap_epsg3035_v1.1.tif',
    'http://92.111.153.194:9000/eumap/lcv/lcv_nir_landsat.glad.ard_p50_30m_0..0cm_2018.06.25..2018.09.12_eumap_epsg3035_v1.1.tif',
    'http://92.111.153.194:9000/eumap/lcv/lcv_swir1_landsat.glad.ard_p50_30m_0..0cm_2018.06.25..2018.09.12_eumap_epsg3035_v1.1.tif',
    'http://92.111.153.194:9000/eumap/lcv/lcv_swir2_landsat.glad.ard_p50_30m_0..0cm_2018.06.25..2018.09.12_eumap_epsg3035_v1.1.tif',
    'http://92.111.153.194:9000/eumap/lcv/lcv_thermal_landsat.glad.ard_p50_30m_0..0cm_2018.06.25..2018.09.12_eumap_epsg3035_v1.1.tif',
    'http://192.168.1.57:9000/eumap/lcv/lcv_ndvi_landsat.glad.ard_p50_30m_0..0cm_2018.06.25..2018.09.12_eumap_epsg3035_v1.1.tif',
    'http://192.168.1.57:9000/eumap/lcv/lcv_ndvi_landsat.glad.ard_p50_30m_0..0cm_2017.12.02..2018.03.20_eumap_epsg3035_v1.1.tif'
]

input_urls_30m = input_urls_landsat + [
    'http://192.168.1.57:9000/eumap/dtm/dtm_cost.distance.to.coast_gedi.grass.gis_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif',
    'http://192.168.1.57:9000/eumap/dtm/dtm_elev.lowestmode_gedi.eml_m_30m_0..0cm_2000..2018_eumap_epsg3035_v0.2.tif',
    'http://192.168.1.57:9000/eumap/hyd/hyd_surface.water_jrc.gswe_p_30m_0..0cm_1984..2019_eumap_epsg3035_v0.1.tif',


]

input_urls_planet_uk = {
    'train' : [
		[
		'http://92.111.153.194:9000/tmp/planet/30N_28E-241N_2018-06-15_red.tif',
		'http://92.111.153.194:9000/tmp/planet/30N_28E-241N_2018-06-15_green.tif',
		'http://92.111.153.194:9000/tmp/planet/30N_28E-241N_2018-06-15_blue.tif',
		'http://92.111.153.194:9000/tmp/planet/30N_28E-241N_2018-06-15_nir.tif'
		],

		[
		'http://92.111.153.194:9000/tmp/planet/30N_27E-236N_2018-06-15_red.tif',
		'http://92.111.153.194:9000/tmp/planet/30N_27E-236N_2018-06-15_green.tif',
		'http://92.111.153.194:9000/tmp/planet/30N_27E-236N_2018-06-15_blue.tif',
		'http://92.111.153.194:9000/tmp/planet/30N_27E-236N_2018-06-15_nir.tif'
		],

		[
		'http://92.111.153.194:9000/tmp/planet/30N_27E-235N_2018-06-15_red.tif',
		'http://92.111.153.194:9000/tmp/planet/30N_27E-235N_2018-06-15_green.tif',
		'http://92.111.153.194:9000/tmp/planet/30N_27E-235N_2018-06-15_blue.tif',
		'http://92.111.153.194:9000/tmp/planet/30N_27E-235N_2018-06-15_nir.tif'
		]
	],
    'valid' : [
		[
		'http://92.111.153.194:9000/tmp/planet/30N_27E-241N_2018-06-15_red.tif',
		'http://92.111.153.194:9000/tmp/planet/30N_27E-241N_2018-06-15_green.tif',
		'http://92.111.153.194:9000/tmp/planet/30N_27E-241N_2018-06-15_blue.tif',
		'http://92.111.153.194:9000/tmp/planet/30N_27E-241N_2018-06-15_nir.tif'
		],

		[
		'http://92.111.153.194:9000/tmp/planet/30N_26E-236N_2018-06-15_red.tif',
		'http://92.111.153.194:9000/tmp/planet/30N_26E-236N_2018-06-15_green.tif',
		'http://92.111.153.194:9000/tmp/planet/30N_26E-236N_2018-06-15_blue.tif',
		'http://92.111.153.194:9000/tmp/planet/30N_26E-236N_2018-06-15_nir.tif',
		]
]}

@dc.dataclass(repr=False)
class TrainingCfg:
    path_tiles: Union[str, Path]
    input_urls: Iterable[Union[str, Path]]
    epochs: int
    bs: int
    target: str
    workers: int
    stride_train: int
    stride_valid: int
    model_name: str
    legend_key: dict
    aggregator: dict
    window_size: int
    downsample: int

    def __getitem__(self, key):
        return self.__dict__[key]

    def __repr__(self):
        return 'Luisanet training configuration\n' + \
            '-------------------------------\n' + \
            '\n'.join([
                f'{k}:'.ljust(15)+v.__repr__()
                for k, v in dc.asdict(self).items()
            ])

luisanet_lvl2_uk_landsat_hydra = TrainingCfg(**{
    'path_tiles' : 'aux-data/tiles_uk.gpkg',
    'input_urls' : input_urls_landsat,
    'epochs':100,
    'bs':150,
    'target' : 'luisa_2',
    'workers' : 50,
    'stride_train' : 10,
    'stride_valid' : 25,
    'model_name' : 'luisanet_lvl2_uk_landsat_hydra',
    'legend_key' : legends.luisa2_to_idx,
    'aggregator' : legends.luisa4_to_luisa2,
    'window_size': 128,
    'downsample': 1})

luisanet_lvl4_uk_landsat_hydra = TrainingCfg(**{
    'path_tiles' : 'aux-data/tiles_uk.gpkg',
    'input_urls' : input_urls_landsat,
    'epochs':100,
    'bs':150,
    'target' : 'luisa_4',
    'workers' : 50,
    'stride_train' : 10,
    'stride_valid' : 25,
    'legend_key' : legends.luisa4_to_idx,
    'aggregator' : None,
    'model_name' : 'luisanet_lvl4_uk_landsat_hydra',
    'window_size': 128,
    'downsample':1})

luisanet_lvl2_all_landsat_hydra = TrainingCfg(**{
    'path_tiles' : 'aux-data/planet_bboxes_epsg3035_aligned_train_test_v0.1.gpkg',
    'input_urls' : input_urls_landsat,
    'epochs':100,
    'bs':150,
    'target' : 'luisa_2',
    'workers' : 50,
    'stride_train' : 10,
    'stride_valid' : 25,
    'legend_key' : legends.luisa2_to_idx,
    'aggregator' : legends.luisa4_to_luisa2,
    'model_name' : 'luisanet_lvl2_all_landsat_hydra',
    'window_size': 128,
    'downsample':1})

luisanet_lvl4_all_landsat_hydra = TrainingCfg(**{
    'path_tiles' : 'aux-data/planet_bboxes_epsg3035_aligned_train_test_v0.1.gpkg',
    'input_urls' : input_urls_landsat,
    'epochs':100,
    'bs':150,
    'target' : 'luisa_4',
    'workers' : 50,
    'stride_train' : 10,
    'stride_valid' : 25,
    'legend_key' : legends.luisa4_to_idx,
    'aggregator' : None,
    'model_name' : 'luisanet_lvl4_all_landsat_hydra',
    'window_size': 128,
    'downsample':1})

luisanet_lvl2_uk_planet_hydra = TrainingCfg(**{
    'path_tiles' : 'aux-data/planet_bboxes_epsg3035_aligned_train_test_v0.1.gpkg',
    'input_urls' : input_urls_planet_uk,
    'epochs':100,
    'bs':50,
    'target' : 'luisa_2',
    'workers' : 15,
    'stride_train' : 10,
    'stride_valid' : 25,
    'model_name' : 'luisanet_lvl2_uk_planet_hydra',
    'legend_key' : legends.luisa2_to_idx,
    'aggregator' : legends.luisa4_to_luisa2,
    'window_size' : 20, #13     20     30
    'downsample': 181}) #118    181    271

luisanet_lvl1_nl_landsat_hydra = TrainingCfg(**{
    'path_tiles' : 'aux-data/tiles_nl.gpkg',
    'input_urls' : input_urls_landsat,
    'epochs':10,
    'bs':100,
    'target' : 'luisa_1',
    'workers' : 50,
    'stride_train' : 128,
    'stride_valid' : 300,
    'legend_key' : legends.luisa1_to_idx,
    'aggregator' : legends.luisa4_to_luisa1,
    'model_name' : 'luisanet_lvl1_nl_landsat_hydra',
    'window_size': 128,
    'downsample':1})

luisanet_lvl2_nl_landsat_hydra = TrainingCfg(**{
    'path_tiles' : 'aux-data/tiles_nl.gpkg',
    'input_urls' : input_urls_landsat,
    'epochs':100,
    'bs':100,
    'target' : 'luisa_2',
    'workers' : 50,
    'stride_train' : 128,
    'stride_valid' : 300,
    'legend_key' : legends.luisa2_to_idx,
    'aggregator' : legends.luisa4_to_luisa2,
    'model_name' : 'luisanet_lvl2_nl_landsat_hydra',
    'window_size': 128,
    'downsample':1})

luisanet_lvl2_nl_30m_hydra = TrainingCfg(**{
    'path_tiles' : 'aux-data/tiles_nl.gpkg',
    'input_urls' : input_urls_30m,
    'epochs':10,
    'bs':100,
    'target' : 'luisa_2',
    'workers' : 50,
    'stride_train' : 128,
    'stride_valid' : 128,
    'legend_key' : legends.luisa2_to_idx,
    'aggregator' : legends.luisa4_to_luisa2,
    'model_name' : 'luisanet_lvl2_nl_30m_hydra',
    'window_size': 128,
    'downsample':1})

luisanet_lvl1_nl_30m_hydra = TrainingCfg(**{
    'path_tiles' : 'aux-data/tiles_nl.gpkg',
    'input_urls' : input_urls_30m,
    'epochs':10,
    'bs':100,
    'target' : 'luisa_1',
    'workers' : 50,
    'stride_train' : 5,
    'stride_valid' : 128,
    'legend_key' : legends.luisa1_to_idx,
    'aggregator' : legends.luisa4_to_luisa1,
    'model_name' : 'luisanet_lvl1_nl_30m_hydra',
    'window_size': 128,
    'downsample':1})

luisanet_lvl1_uk_landsat_hydra = TrainingCfg(**{
    'path_tiles' : 'aux-data/tiles_uk.gpkg',
    'input_urls' : input_urls_landsat,
    'epochs':20,
    'bs':10,
    'target' : 'luisa_1',
    'workers' : 50,
    'stride_train' : 128,
    'stride_valid' : 128,
    'legend_key' : legends.luisa1_to_idx,
    'aggregator' : legends.luisa4_to_luisa1,
    'model_name' : 'luisanet_lvl1_uk_landsat_hydra',
    'window_size': 128,
    'downsample':1})

luisanet_grass_nl_30m_hydra = TrainingCfg(**{
    'path_tiles' : 'aux-data/tiles_nl.gpkg',
    'input_urls' : input_urls_30m,
    'epochs':100,
    'bs':100,
    'target' : 'grass1',
    'workers' : 50,
    'stride_train' : 31,
    'stride_valid' : 128,
    'legend_key' : legends.grass1_to_idx,
    'aggregator' : legends.luisa4_to_grass1,
    'model_name' : 'luisanet_grass_nl_30m_hydra',
    'window_size': 128,
    'downsample':1})

luisanet_lvl1_eu_30m_hydra = TrainingCfg(**{
    'path_tiles' : 'aux-data/eu_tiling system_30km.gpkg',
    'input_urls' : input_urls_30m,
    'epochs':100,
    'bs':10,
    'target' : 'luisa_1',
    'workers' : 50,
    'stride_train' : 128,
    'stride_valid' : 128,
    'legend_key' : legends.luisa1_to_idx,
    'aggregator' : legends.luisa4_to_luisa1,
    'model_name' : 'luisanet_lvl1_eu_landsat_hydra',
    'window_size': 128,
    'downsample':1})

luisanet_lvl4_nl_30m_hydra = TrainingCfg(**{
    'path_tiles' : 'aux-data/planet_bboxes_epsg3035_aligned_train_test_v0.1.gpkg',
    'input_urls' : input_urls_landsat,
    'epochs':20,
    'bs':150,
    'target' : 'luisa_4',
    'workers' : 50,
    'stride_train' : 32,
    'stride_valid' : 128,
    'legend_key' : legends.luisa4_to_idx,
    'aggregator' : None,
    'model_name' : 'luisanet_lvl4_nl_30m_hydra',
    'window_size': 128,
    'downsample':1})