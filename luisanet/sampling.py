import rasterio as rio
import numpy as np
import multiprocessing as mp
from multiprocessing.pool import ThreadPool
from itertools import islice

def _read_singleband_raster(window=None):
    def wrapped(url):
        with rio.open(url) as src:
            return src.read(1, window=window)
    return wrapped

def augment_by_index(idx, out_input, out_target):
    flip = idx // 4
    rot_k = idx % 4
    out_target = np.flip(out_target, axis=0)
    out_input = np.flip(out_input, axis=1)
    out_target = np.rot90(out_target, k=rot_k)
    out_input = np.rot90(out_input, k=rot_k, axes=(1, 2))
    return out_input, out_target

def _augment(out_input, out_target):
    for i in range(8):
        yield augment_by_index(i, out_input, out_target)

def _batchgen(
    sampler,
    batch_size=1000,
    **kwargs,
):
    samplegen = sampler.samplegen(**kwargs)
    batch_i = 0
    while True:
        batch = [*islice(
            samplegen,
            batch_i*batch_size,
            (batch_i+1)*batch_size,
        )]
        if batch == []:
            break
        try:
            assert kwargs['return_indices']
            batch_indices, batch_input, batch_target = map(
                np.stack,
                zip(*batch),
            )
            yield batch_indices, batch_input, batch_target
            batch_i += 1
        except (KeyError, AssertionError):
            batch_input, batch_target = map(
                np.stack,
                zip(*batch),
            )
            yield batch_input, batch_target
            batch_i += 1

class WindowSampler:
    target_url = 'http://92.111.153.194:9000/eumap/lcv/lcv_landcover_luisa_jrc_30m_0..0cm_2018_eumap_epsg3035_v0.1.tif'

    def __init__(self, input_urls, window=None):
        with rio.open(self.target_url) as target_src:
            with rio.open(input_urls[0]) as input_src:
                self.resolution = target_src.transform[0]
                self.input_resolution = input_src.transform[0]

                if window is None:
                    self.input_window = None
                    self.window = rio.windows.from_bounds(
                        *input_src.bounds,
                        transform=target_src.transform,
                    )
                else:
                    if isinstance(window, rio.windows.Window):
                        bounds = target_src.window_bounds(window)
                    else:
                        bounds = window
                        window = rio.windows.from_bounds(
                            *window,
                            transform=target_src.transform,
                        )

                    self.input_window = rio.windows.from_bounds(
                        *bounds,
                        transform=input_src.transform,
                    )

                    self.window = window

                self.w = int(self.window.width)
                self.h = int(self.window.height)
                self.transform = target_src.window_transform(self.window)
                self.profile = {
                    **target_src.profile,
                    'transform': self.transform,
                    'width': self.w,
                    'height': self.h,
                }

        self.target_data = _read_singleband_raster(self.window)(self.target_url)
        with ThreadPool(mp.cpu_count()) as pool:
            self.input_data = np.stack(pool.map(
                _read_singleband_raster(self.input_window),
                input_urls,
            ))

    def _get_sample(self, i0, j0, window_dim, fill_value=0):
        i1 = i0 + window_dim
        j1 = j0 + window_dim
        out_target = np.full(
            (window_dim, window_dim),
            fill_value=fill_value,
        )
        target_sample = self.target_data[i0:i1,j0:j1]
        out_target[
            :target_sample.shape[0],
            :target_sample.shape[1],
        ] = target_sample

        _i0, _i1, _j0, _j1, _window_dim = (np.array([
            i0, i1, j0, j1, window_dim
        ]) * self.resolution / self.input_resolution).astype(int)

        out_input = np.full(
            (self.input_data.shape[0], _window_dim, _window_dim),
            fill_value=fill_value,
        )
        input_sample = self.input_data[:,_i0:_i1,_j0:_j1]
        out_input[
            :,
            :input_sample.shape[1],
            :input_sample.shape[2],
        ] = input_sample
        return out_input, out_target

    def _samplegen(self,
        window_dim=128,
        fill_value=0,
        return_indices=False,
        augment=False,
    ):
        n_x = self.target_data.shape[1] // window_dim + bool(self.target_data.shape[1] % window_dim)
        n_y = self.target_data.shape[0] // window_dim + bool(self.target_data.shape[0] % window_dim)
        for i in range(n_y):
            i0 = i * window_dim
            for j in range(n_x):
                j0 = j * window_dim
                out_input, out_target = self._get_sample(
                    i0, j0, window_dim,
                    fill_value=fill_value,
                )
                if augment:
                    _out = _augment(out_input, out_target)
                else:
                    _out = [[out_input, out_target]]
                for sample in _out:
                    if return_indices:
                        yield [(i0, j0), *sample]
                    else:
                        yield sample

    def _samplegen_moving(self,
        window_dim=128,
        fill_value=0,
        return_indices=False,
        augment=False,
    ):
        for i0 in range(self.target_data.shape[0]-window_dim):
            for j0 in range(self.target_data.shape[1]-window_dim):
                out_input, out_target = self._get_sample(
                    i0, j0, window_dim,
                    fill_value=fill_value,
                )
                if augment:
                    _out = _augment(out_input, out_target)
                else:
                    _out = [[out_input, out_target]]
                for sample in _out:
                    if return_indices:
                        yield [(i0, j0), *sample]
                    else:
                        yield sample

    def samplegen(self,
        window_dim=128,
        fill_value=0,
        return_indices=False,
        augment=False,
        moving=True,
    ):
        if moving:
            _sampler = self._samplegen_moving
        else:
            _sampler = self._samplegen
        for sample in _sampler(
            window_dim=window_dim,
            fill_value=fill_value,
            return_indices=return_indices,
            augment=augment,
        ):
            yield sample

    def batchgen(self,
        batch_size=1000,
        **kwargs,
    ):
        return _batchgen(
            self,
            batch_size=batch_size,
            **kwargs,
        )


class MultiWindowSampler:
    def __init__(self, input_urls, windows=None):
        if windows is None:
            self.subsamplers = [
                WindowSampler(input_url_group, window=None)
                for input_url_group in input_urls
            ]
        else:
            self.subsamplers = [
                WindowSampler(input_urls, window=window)
                for window in windows
            ]
        self.target_data = self.subsamplers[0].target_data

    def samplegen(self,
        window_dim=128,
        fill_value=0,
        return_indices=False,
        augment=False,
        moving=False,
    ):
        samplegens = [
            sampler.samplegen(
                window_dim=window_dim,
                fill_value=fill_value,
                return_indices=return_indices,
                augment=augment,
                moving=moving,
            )
            for sampler in self.subsamplers
        ]
        for samples in zip(*samplegens):
            for sample in samples:
                yield sample

    def batchgen(self,
        batch_size=1000,
        **kwargs,
    ):
        return _batchgen(
            self,
            batch_size=batch_size,
            **kwargs,
        )
