import multiprocessing as mp
from multiprocessing.dummy import Pool as ThreadPool
from tqdm import tqdm
import numpy as np
import torch

def summary(arr,argmax=True):
    if torch.is_tensor(arr):
        arr = arr.cpu().detach().numpy()
    if argmax:
        arr = np.argmax(arr,1)
    info = np.unique(arr,return_counts=True)
    info = np.column_stack(info)
    try:
        info = info.astype(int)
    except ValueError:
        pass

    return(info)

def do_parallel(func,tasks,use_chunks=False,progress=True,workers=None,desc="",mode='cores'):

    # Tasks involving I/O can benefit from having more workers than available cores
    workers = np.min([len(tasks),mp.cpu_count()-1]) if workers is None else workers
    print(workers,'workers')
    if use_chunks:
        n_chunks = len(tasks) // workers
        tasks = [[func,tasks[i:i + n_chunks]] for i in range(0, len(tasks), n_chunks)]
        for task in tasks:
            print(len(task[1]))
        func = process_task_chunk
    if mode == 'cores':
        pool = mp.Pool
    elif mode == 'threads':
        pool = ThreadPool
    with pool(workers) as p:
        if progress:
            r = list(tqdm(p.imap(func,tasks),total=len(tasks),desc=desc))
        else:
            r = p.map(func,tasks)
    if all(i is None for i in r):
        return(True)
    else:
        return(r)

def process_task_chunk(task_chunk):
    function, tasks = task_chunk
    r = []
    for task in tasks:
        r.append(function(task))
    return r


def ttprint(*args, **kwargs):
    from datetime import datetime
    import sys

    ts = f'[{datetime.now():%H:%M:%S}]'

    first, *rest = args
    if isinstance(first, str) and first.startswith('\n'):
        first = first[1:]
        ts = '\n' + ts

    args = (ts, first, *rest)

    print(*args, **kwargs, flush=True)
