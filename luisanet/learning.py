import torch
from tqdm import tqdm
# import torch.onnx as onnx
import os
import numpy as np
import joblib
from . import legends, accuracy
from glob import glob
import pandas as pd

def hierarchical_loss(y_pred,y_true,keys,weights,criterion):
    losses = []
    for i in range(len(keys)):
        y_pred = legends.reclass(y_pred,keys[i])
        y_true = legends.reclass(y_true,keys[i])
        loss_i = criterion(y_pred,y_true)
        losses += [loss_i]*weights[i]
    assert len(losses) == np.sum(weights)
    return np.mean(losses)


def calculate_loss_overfit(loss_train,loss_valid):
    overfit = loss_train - loss_valid
    return overfit

def calculate_score_overfit(score_train,score_valid):
    overfit = score_valid - score_train
    return overfit

def get_best_model(model_name):
    paths_existing = sorted(glob(model_name + "_*.pth"))
    path_best_existing = paths_existing[0] if len(paths_existing) > 0 else None
    return path_best_existing

def get_best_checkpoint(model_name):
    paths_existing = sorted(glob('checkpoints/' + model_name + "_*.pth"))
    path_best_existing = paths_existing[0] if len(paths_existing) > 0 else None
    return path_best_existing

def unpack_batch(data,device):
    if type(data[0]) is list:
        x = [data[0][i].to(device) for i in range(len(data[0]))]
    else:
        x = data[0].to(device)
        y = data[1].to(device).squeeze().long()
    return x, y 

def predict(model,x):
    logits = model(x)
    y_pred = torch.nn.functional.softmax(logits,dim=1)
    return y_pred

def calculate_loss(x,y_true,model,criterion,verbose=False):
    logits = model(x)
    y_pred = torch.nn.functional.softmax(logits,dim=1)
    # y_pred = predict(model,x)
    loss = criterion(y_pred,y_true)
    # print('y_pred:\n',summary(y_pred,argmax=True),'\n')
    # print('y_true:\n',summary(y_true,argmax=False),'\n')

    if verbose:
        print('x:',x.shape,type(x),x.dtype)
        print('y_true',y_true.shape,len(np.unique(y_true)))
        print('y_pred:',y_true.shape,type(y_true),y_true.dtype)
        print('logits', logits.shape, torch.min(logits), torch.max(logits))
        print('softmax', y_pred.shape, torch.min(y_pred), torch.max(y_pred))
        print('loss',loss)

    return loss

def get_lr(optimizer):
    for param_group in optimizer.param_groups:
        lr = param_group['lr']
        return lr

def save_checkpoint(path_checkpoint, epoch, model,optimizer,info,lr_scheduler=None):
    checkpoint = { 
    'epoch': epoch,
    'model': model.state_dict(),
    'optimizer': optimizer.state_dict(),
    'info': info,
    # 'lr_scheduler': lr_scheduler
    }
    torch.save(checkpoint, path_checkpoint)

def load_checkpoint(path_checkpoint):
    c = torch.load(path_checkpoint)
    return c['epoch'],c['model'],c['optimizer'], c['info'] #,c['lr_scheduler']

def train(
    model=None,criterion=None,optimizer=None,
    dl_train=None,dl_valid=None,device='gpu:0',epochs=10,
    step_size=None,model_name='luisanet',
    lr=0.001,momentum=0.9,lower_lr_rounds=None,lower_lr_factor=2,
    verbose=False,path_checkpoint=None,stride=None,image_size=None):

    dir_checkpoints = './checkpoints'; os.makedirs(dir_checkpoints,exist_ok=True)

    if path_checkpoint is not None:
        last_epoch, model_state_dict, optimizer_state_dict, info = load_checkpoint(path_checkpoint)
        epoch_start = last_epoch +1
        info = info.to_dict('list')
        loss_best = info['loss_valid'][-1]
        epochs_since_improvement = info['epochs_since_improvement'][-1]
        model.load_state_dict(model_state_dict)
        optimizer.load_state_dict(optimizer_state_dict)
        print(optimizer,type(optimizer))
        print("Loaded checkpoint from",path_checkpoint)
        path_best = path_checkpoint
    else:
        info = {
            'epoch':[], 'learning_rate':[],
            'loss_train':[],'loss_valid':[],
            'epochs_since_improvement':[],
            'f1_train':[],'f1_valid':[],
            'stride':[],'image_size':[]}
        loss_best = 100; path_best = None; epoch_start = 0

    # Export the untrained network in Open Neural Network Exchange format
    # dummy_input = iter(dl_train).next()
    # path_model_untrained = f"./{model_name}_untrained.onxx"
    # path_model_trained = f"./{model_name}_trained.onxx"
    # onnx.export(model,dummy_input[0][0].cuda(),dummy_input[0][1].cuda(),path_model_untrained,verbose=False)
    
    for epoch in range(epoch_start,epochs+epoch_start):
        print('Epoch',epoch)
        info['epoch'].append(epoch); info['learning_rate'].append(get_lr(optimizer))
        info['stride'].append(step_size); info['image_size'].append(image_size)
        # Prepare to train
        loss_train = 0.0
        f1_train = 0.0
        model = model.train()

        ## train on each batch from the training dataloader
        for i, data in tqdm(
            enumerate(dl_train), desc=f'Train',total=len(dl_train)):
            # Train on batch
            x, y_true = unpack_batch(data,device)
            optimizer.zero_grad()
            y_pred = predict(model,x)
            loss = accuracy.cross_entropy(y_true,y_pred)
            loss.backward()
            optimizer.step()            
            f1 = accuracy.f1_score(y_true,y_pred)
            loss_train += loss.item()
            f1_train += f1
        
        # Divide by number of batches to get average epoch loss
        loss_train /= i; f1_train /= i
        info['loss_train'].append(loss_train); info['f1_train'].append(f1_train)

        # Validate if a validation dataloader was provided
        if dl_valid is not None:
            # prepare to validate
            loss_valid = 0.0; f1_valid = 0.0; model.eval()
            with torch.no_grad():
                # validate on each batch from the validation dataloader
                for i, data in tqdm(
                    enumerate(dl_valid),desc=f"Valid",
                    total=len(dl_valid)):
                    x, y_true = unpack_batch(data,device)
                    y_pred = predict(model,x)
                    loss = accuracy.cross_entropy(y_true,y_pred)
                    loss_valid += loss
                    f1_valid += accuracy.f1_score(y_true,y_pred)
                # Divide by number of batches to get average epoch validation loss
                loss_valid /= i
                f1_valid /= i
                info['loss_valid'].append(loss_valid.item())
                info['f1_valid'].append(f1_valid)
                print(f"\ttrain: {loss_train:.3f} valid: {loss_valid:.3f} best: {loss_best:.3f}")
        else:
            loss_valid = loss_train
            print(f"\ttrain: {loss_train:.3f}")
                
        # Save this epoch's model if it performed better than the previous best
        if loss_valid <= loss_best:
            path_best_previous = path_best
            path_best = f'{dir_checkpoints}/{model_name}_tmp_{loss_valid:.3f}_e{epoch}.pth'
            loss_best = loss_valid
            epochs_since_improvement = 0
            if path_best_previous is not None:
                if os.path.exists(path_best_previous):
                    os.remove(path_best_previous)
            info['epochs_since_improvement'].append(epochs_since_improvement)
            save_checkpoint(path_best,epoch,model,optimizer,pd.DataFrame(info))
        else:
            epochs_since_improvement +=1
            info['epochs_since_improvement'].append(epochs_since_improvement)
        
        df = pd.DataFrame(info)
        df.to_csv(f'info_{model_name}.csv')
    
    # Save the final model
    # onnx.export(model,dummy_input[0].cuda(),path_model_trained,verbose=False)
    return model


