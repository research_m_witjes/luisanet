from glob import glob
import torch
import numpy as np
import geopandas as gpd

from ..dataset import LuisaDataset
from .. import dataset, learning, architectures, config
from ..learning import train
from ..legends import reclass, luisa4_to_idx, luisa2_to_idx, luisa4_to_luisa2

if __name__ == '__main__':

    # Training settings
    # c = config.luisanet_lvl2_nl_30m_hydra
    # c = config.luisanet_lvl1_uk_landsat_hydra
    # c = config.luisanet_grass_nl_30m_hydra
    c = config.luisanet_lvl1_eu_30m_hydra
    c = config.luisanet_lvl1_nl_30m_hydra

    # Get tiles to train & validate
    tiles = gpd.read_file(c.path_tiles)
    try:
        len(tiles.train)
    except:
        tiles['train'] = np.random.choice([1,0],size=len(tiles),replace=True,p=[0.8,0.2])

    tiles_train = tiles.loc[tiles.train == 1]
    tiles_valid = tiles.loc[tiles.train == 0]
    bounds_train = [tiles_train.geometry[i].bounds for i in tiles_train.index]
    bounds_valid = [tiles_valid.geometry[i].bounds for i in tiles_valid.index]


    dst = dataset.LuisaDataset(
        c.input_urls,
        bounds_train,
        legend_key = c.legend_key,
        aggregator = c.aggregator,
        transforms=8,
        stride=c.stride_train)

    dsv = dataset.LuisaDataset(
        c.input_urls,
        bounds_valid,
        legend_key = c.legend_key,
        aggregator = c.aggregator,
        transforms=8,
        stride=c.stride_valid)

    dlt = torch.utils.data.DataLoader(
        dst,
        num_workers=c.workers,
        batch_size=c.bs,
        shuffle=True,
        pin_memory=True)

    dlv = torch.utils.data.DataLoader(
        dsv,
        batch_size=c.bs,
        num_workers=c.workers,
        pin_memory=True)

    print('training dataset length:',len(dst))
    print(f'with batch size {c.bs} each of {c.epochs} epochs will take {int(len(dst)/c.bs)} steps')
    n_channels = dst.n_bands
    n_classes = len(set(c['legend_key'].values()))
    print(n_classes,'classes')
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print('device:',device)
    criterion = torch.nn.CrossEntropyLoss()
    model = architectures.UNet(n_channels,n_classes,downsample=1).to(device)
    optimizer = torch.optim.Adam(model.parameters(),lr=0.0001)

    # Export the untrained network in Open Neural Network Exchange format
    # onnx.export(model,dummy_input[0].cuda(),'hydranet.onnx',verbose=False)
    
    
    train(
        path_checkpoint=learning.get_best_checkpoint(c.model_name),
        model = model,
        criterion = criterion,
        optimizer = optimizer,
        dl_train = dlt,
        dl_valid = dlv,
        device=device,
        epochs=c.epochs,
        model_name=c.model_name,
        step_size=c.stride_train,
        image_size=c.window_size
    )
    


