import geopandas as gpd
import torch
import numpy as np

from ..dataset import LuisaDataset
from .. import dataset, learning, architectures, config
from ..learning import train
from ..legends import reclass, luisa4_to_idx, luisa2_to_idx, luisa4_to_luisa2

# https://nbviewer.org/github/skorch-dev/skorch/blob/master/examples/nuclei_image_segmentation/Nuclei_Image_Segmentation.ipynb

if __name__ == '__main__':

    # Training settings
    c = config.luisanet_lvl2_uk_planet_hydra
    print(c)
    bs = c['bs']
    epochs = c['epochs']
    workers = c['workers']
    target = c['target']
    stride_train = c['stride_train']
    stride_valid = c['stride_valid']
    input_urls = c['input_urls']
    legend_key = c['legend_key']
    aggregator = c['aggregator']

    # input_urls_planet = [
    #     '/home/luka/code/m1/luisanet/tmp/planet_3035_aligned/30N_26E-236N_2018-06-15_blue.tif',
    #     '/home/luka/code/m1/luisanet/tmp/planet_3035_aligned/30N_26E-236N_2018-06-15_green.tif',
    #     '/home/luka/code/m1/luisanet/tmp/planet_3035_aligned/30N_26E-236N_2018-06-15_red.tif',
    #     '/home/luka/code/m1/luisanet/tmp/planet_3035_aligned/30N_26E-236N_2018-06-15_nir.tif',
    #     '/home/luka/code/m1/luisanet/tmp/planet_3035_aligned/30N_27E-234N_2018-06-15_blue.tif',
    #     '/home/luka/code/m1/luisanet/tmp/planet_3035_aligned/30N_27E-234N_2018-06-15_green.tif',
    #     '/home/luka/code/m1/luisanet/tmp/planet_3035_aligned/30N_27E-234N_2018-06-15_red.tif',
    #     '/home/luka/code/m1/luisanet/tmp/planet_3035_aligned/30N_27E-234N_2018-06-15_nir.tif',
    #     '/home/luka/code/m1/luisanet/tmp/planet_3035_aligned/30N_27E-235N_2018-06-15_blue.tif',
    #     '/home/luka/code/m1/luisanet/tmp/planet_3035_aligned/30N_27E-235N_2018-06-15_green.tif',
    #     '/home/luka/code/m1/luisanet/tmp/planet_3035_aligned/30N_27E-235N_2018-06-15_red.tif',
    #     '/home/luka/code/m1/luisanet/tmp/planet_3035_aligned/30N_27E-235N_2018-06-15_nir.tif',
    # ]
    # input_urls_planet = np.array(input_urls_planet).reshape(-1, 4)

    dst = dataset.LuisaDataset(
        input_urls['train'],
        legend_key = legend_key,
        aggregator = aggregator,
        window_dim = c['window_size'],
        bands = 4,
        transforms=8,
        stride=stride_train)

    dsv = dataset.LuisaDataset(
        input_urls['valid'],
        legend_key = legend_key,
        aggregator = aggregator,
        window_dim = c['window_size'],
        bands = 4,
        transforms=8,
        stride=stride_valid)

    dlt = torch.utils.data.DataLoader(
        dst,
        num_workers=workers,
        batch_size=bs,
        shuffle=True,
        pin_memory=True)

    dlv = torch.utils.data.DataLoader(
        dsv,
        batch_size=bs,
        num_workers=workers,
        pin_memory=True)

    print('training dataset length:',len(dst))
    print(f'with batch size {bs} each of {epochs} epochs will take {int(len(dst)/bs)} steps')



    n_channels = dst.n_bands
    print(n_channels)
    n_classes = len(set(legend_key.values()))




    print(n_classes,'classes')
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    # device = torch.device("cpu")
    print('device:',device)
    criterion = torch.nn.CrossEntropyLoss()
    model = architectures.UNet(n_channels,n_classes,downsample=c['downsample']).to(device)
    optimizer = torch.optim.Adam(model.parameters(),lr=0.001)

    # Export the untrained network in Open Neural Network Exchange format
    # onnx.export(model,dummy_input[0].cuda(),'hydranet.onnx',verbose=False)

    train(model=model,
        criterion=criterion,
        optimizer = optimizer,
        dl_train=dlt,
        dl_valid=dlv,
        device=device,
        epochs=epochs,
        model_name=c['model_name'])
