import torch
import multiprocessing as mp
import numpy as np
import torchvision as tv


from .. dataset import MultiDataset
from .. import transformations as trfm
from ..config import (
        project_name,
        dir_storage_slow,
        dir_storage_fast,
        dir_preprocessing,
        dir_deep_learning,
        path_points_source,
        path_nl,
        dir_landsat,
        dir_ahn,
        path_points_preprocessed,
        dir_blocks,
)

if __name__ == '__main__':

    bs = 1
    ds = MultiDataset(
        path_points=path_points_preprocessed,
        y='clc_1',
        point_columns_x=['lcv_thermal_landsat.glad.ard_p75_30m_0..0cm_.09.13...12.01_eumap_epsg3035_v1.1'],
        window=10,
        elevation=True,
        transform=tv.transforms.Compose([trfm.ToTensor()]),
        dir_blocks=dir_blocks)

    dl = torch.utils.data.DataLoader(ds,batch_size=bs,shuffle=True,num_workers=np.min([bs,mp.cpu_count()]))

    dummy_input = iter(dl).next()
    n_classes = len(ds.points[ds.y].unique())

    for i in dummy_input:
        print(type(i))
        if type(i) is list:
            for j in i:
                print('\t',type(j))
