import argparse
import geopandas as gpd
import joblib
import numpy as np
import rasterio as rio
import multiprocessing as mp
import numpy as np
import os
import pandas as pd
import random
import shutil
from tqdm import tqdm

# Custom libraries
from .. import legends
from .. import raster
from .. import util
from ..config import (
        project_name,
        dir_storage_slow,
        dir_storage_fast,
        dir_preprocessing,
        dir_deep_learning,
        path_points_source,
        path_nl,
        dir_landsat,
        dir_ahn,
        path_points_preprocessed,
        dir_blocks,
)

if __name__ == '__main__':

    p = argparse.ArgumentParser()
    p.add_argument('--overwrite_points',default=False,action='store_true')
    p.add_argument('--overwrite_tasks',default=False,action='store_true')
    p.add_argument('--overwrite_blocks',default=False,action='store_true')
    a = p.parse_args()

    pts = None
    if a.overwrite_points:
        # Load the netherlands vector and points
        nl = gpd.read_file(path_nl).to_crs(epsg=28992)
        pts = joblib.load(path_points_source).to_crs(epsg=28992)


        # Isolate points in NL
        pts = gpd.clip(pts,nl.to_crs(pts.crs))


        # Put s2glc landcover in the points
        #TODO change to clc_to_s2glc
        pts['s2glc'] = ''
        for lc1 in legends.lucas_to_s2glc:
            pts.s2glc.loc[pts.lc1 == lc1] = legends.lucas_to_s2glc[lc1]

        # print(pts.s2glc.value_counts().sort_index())
        # Create three hierarchical corine classification levels
        pts.dropna(subset=['clc3_orig'])
        print(pts.clc3_orig.value_counts().sort_index())
        pts['clc_3'] = pts.clc3_orig.astype(str).str[:3]
        pts = pts.loc[pts.clc_3 != 'nan']
        pts['clc_2'] = pts.clc_3.str[:2]
        pts['clc_1'] = pts.clc_3.str[0]

        print(pts.clc_3.value_counts().sort_index())
        print(pts.clc_2.value_counts().sort_index())
        print(pts.clc_1.value_counts().sort_index())


        # Create three hierarchical lucas classification levels
        pts['lucas_3'] = pts.lc1
        pts['lucas_2'] = pts.lc1.str[:2]
        pts['lucas_1'] = pts.lc1.str[:1]

        print(pts.lucas_3.value_counts().sort_index())
        print(pts.lucas_2.value_counts().sort_index())
        print(pts.lucas_1.value_counts().sort_index())

        # Add survey year
        pts['survey_year'] = pts.survey_date.astype(str).str[:4]
        print(pts.survey_year.value_counts().sort_index())
        for c in pts.columns:
            print(c)

        # Store the preprocessed points
        joblib.dump(pts,path_points_preprocessed,compress='lz4')


    def prepare_features(task,idx=None):
        pt, dir_landsat, dir_ahn, dir_blocks = task

        pt.set_crs(epsg=28992)

        idx = pt.index.values[0] if idx is None else idx
        dir_idx = f'{dir_blocks}/{idx}'
        os.makedirs(dir_idx,exist_ok=True)
        path_30m = f"{dir_idx}/data_30m.joblib"
        path_5m = f'{dir_idx}/data_5m.joblib'
        year = int(pt.survey_year.values[0]) if idx is None else int(pt.survey_year)


        paths = {}
        if not os.path.exists(path_30m):
            for band in ['red','green','blue','nir','swir1','swir2','thermal']:
                for period in [
                    [f'03.21',f'{year}.06.24'],
                    [f'06.25',f'{year}.09.12'],
                    [f'09.13',f'{year}.12.01'],
                    [f'12.02',f'{year+1}.03.20']]:
                    for percentile in [25,50,75]:
                        key = f"{band}_{period[0][:2]}_p{percentile}"
                        paths[key] = f'{dir_landsat}/lcv_{band}_landsat.glad.ard_p{percentile}_30m_0..0cm_{year}.{period[0]}..{period[1]}_eumap_epsg3035_v1.1.tif'

        if not os.path.exists(path_5m):
            ahn_version = 2 if year < 2013 else 3
            ahn_version = 1 if year < 2006 else ahn_version
            paths['ahn'] = f'{dir_ahn}/ahn{ahn_version}_dsm_harmonized.tif'

        pad_5m = np.floor(255/2)
        pad_30m = pad_5m // (30/5)

        arrays_30m = []
        arrays_5m = []
        for key in paths:
            path_tif = paths[key]
            try:
                if '_30m_' in paths[key]:
                    arrays_30m.append(raster.extract_context(pt,path_tif,pad=pad_30m))
                if key == 'ahn':
                    arrays_5m.append(raster.extract_context(pt,path_tif,pad=pad_5m))
            except rio.errors.WindowError:
                print(f"Point {idx} failed, removing directory {dir_idx}")
                shutil.rmtree(dir_idx)

                return pt

        if len(arrays_30m) > 0:
            arrays_30m = np.stack(arrays_30m)
            joblib.dump(arrays_30m, path_30m, compress='lz4')
            arrays_30m = None

        if len(arrays_5m) > 0:
            arrays_5m = np.stack(arrays_5m)
            joblib.dump(arrays_5m,  path_5m, compress='lz4')
            arrays_5m = None


    path_tasks = f"{dir_preprocessing}/blocking_tasks.joblib"
    if a.overwrite_tasks or not os.path.exists(path_tasks):
        pts = joblib.load(path_points_preprocessed)
        tasks = []
        for index in tqdm(pts.index.unique(),desc='preparing tasks'):
            tasks.append([
                gpd.GeoDataFrame(pts.loc[index]).T,
                dir_landsat, dir_ahn,
                dir_blocks])
        random.shuffle(tasks)
        joblib.dump(tasks,path_tasks,compress='lz4')

    else:
        print(f"Loading tasks from {path_tasks}")
        tasks = joblib.load(path_tasks)

    if a.overwrite_blocks or len(os.listdir(dir_blocks)) == 0:
        shutil.rmtree(dir_blocks)
        os.makedirs(dir_blocks)
        block_errors = util.do_parallel(prepare_features,tasks,use_chunks=True)
        joblib.dump(block_errors,f"{dir_preprocessing}/bad_points.joblib",compress='lz4')

    if pts is None:
        pts = joblib.load(path_points_preprocessed)

    bad_points = []
    for chunk in block_errors:
        for point in chunk:
            if point is not None:
                bad_points.append(point)
    bad_points = pd.concat(bad_points)

    pts = pts.drop(bad_points.index)
    joblib.dump(pts,path_points_preprocessed,compress='lz4')
