import argparse
import joblib
import geopandas as gpd
import pandas as pd
import numpy as np
import rasterio as rio
import os
import os.path as osp
from ..raster import overlay
from ..legends import lucas_1, lucas_3, luisa_1, luisa_4

# This script calculates the match between LUCAS points and the LUISA Basemap.
# First it overlays a points geopackage with the column 'lc1' on a version of the LUISA basemap
# It then calculates the class of each point in multiple levels of both legends
# Then it creates a cross-table of both legends, counting the co-occurrence of lucas and luisa classes.
# These crosstables are exported as csv files.

if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument("--n",default=None)
    p.add_argument("--path_points",default="/mnt/atlas/luisanet/lucas_2018.gpkg")
    p.add_argument('--path_points_joblib',default='/mnt/hydra/luisanet/lucas_2018.joblib')
    p.add_argument("--path_y",default='http://92.111.153.194:9000/eumap/lcv/lcv_landcover_luisa_jrc_30m_0..0cm_2018_eumap_epsg3035_v0.1.tif')
    p.add_argument("--path_points_preprocessed",default='/mnt/hydra/luisanet/lucas_2018_preprocessed.gpkg')
    p.add_argument("--dir_crosstab",default='/mnt/hydra/luisanet')
    a = p.parse_args()

    if osp.exists(a.path_points_joblib):
        print("Loading existing joblib archive")
        pts = joblib.load(a.path_points_joblib)

    else:
        print("Loading geopackage")
        pts = gpd.read_file(a.path_points)
        print("Writing joblib archive")
        joblib.dump(pts,a.path_points_joblib,compress='lz4')
    
    if a.n is not None:
        pts = pts.sample(int(a.n))
    
    print("Extracting survey_year")
    pts['survey_year'] = pts.survey_date.str[:4]
    pts = pts.loc[pts.survey_year == '2018']

    # Get the full LUISA class for each point
    print("Overlaying")
    pts = overlay(a.path_y,pts,'luisa_4',verbose=True)
    print("Adding legends")
    for i in range(1,5):
        pts[f"luisa_{i}"] = pts.luisa_4.astype(str).str[:i]

    # Derive LUCAS and LUISA classes and labels
    pts['lucas_1'] = pts.lc1.str[:1]
    pts['lucas_1_lbl'] = pts.lucas_1.replace(lucas_1)
    pts['lucas_3_lbl'] = pts.lc1.replace(lucas_3)
    pts['luisa_1_lbl'] = pts.luisa_1.replace(luisa_1)
    pts['luisa_4_lbl'] = pts.luisa_4.replace(luisa_4)
    
    # Save the points
    print("Saving to",a.path_points_preprocessed)
    pts.to_file(a.path_points_preprocessed)
    print(pts.head())

    # Crosstabulate LUCAS and LUISA classes
    # TODO: remove first row after header (and investigate why it shows up)
    ct_large = pd.crosstab(pts.lucas_3_lbl,pts.luisa_4_lbl)
    ct_small = pd.crosstab(pts.lucas_1_lbl,pts.luisa_1_lbl)
    print(ct_small)
    print(ct_large)
    ct_small.to_csv(osp.join(a.dir_crosstab,'lucas1_luisa1_crosstab.csv'))
    ct_large.to_csv(osp.join(a.dir_crosstab,'lucas3,luisa4_crosstab.csv'))

    
    