#Import libraries
import geopandas as gpd
import joblib
import matplotlib.pyplot as plt
import numpy as np
import torchvision as tv
import torch
import torch.nn as nn
from tqdm import tqdm
import netron
import torch.onnx as onnx
from torch.utils.tensorboard import SummaryWriter
import os
import shutil
import sacred
import transformations as trfm


from ..dataset import LandsatDataset, MultiDataset
from ..architectures import Net
from ..config import (
        project_name,
        dir_storage_slow,
        dir_storage_fast,
        dir_preprocessing,
        dir_deep_learning,
        path_points_source,
        path_nl,
        dir_landsat,
        dir_ahn,
        path_points_preprocessed,
        dir_blocks,
)

if __name__ == '__main__':

    points = joblib.load(path_points_preprocessed).to_crs(epsg=28992)

    # Make Dataset
    # https://pytorch.org/tutorials/beginner/data_loading_tutorial.html


    # Make transformations





    bands = [4,16,28,40,52,64,76]
    transform = tv.transforms.Compose([
        trfm.Rotate(), trfm.Flip(),
        trfm.Normalize(), trfm.ToTensor()])
    transform = None
    batch_size = 128
    epochs = 100
    criterion = nn.CrossEntropyLoss()
    learning_rate = 0.001
    momentum = 0.9
    architecture = Net
    loaders = 40


    ds = LandsatDataset(
        path_points=path_points_preprocessed,
        y='clc_1',
        landsat=False,
        bands=bands,
        window=10,
        elevation=True,
        dir_blocks=dir_blocks,
        transform=transform)



    dl = torch.utils.data.DataLoader(ds,batch_size=batch_size,shuffle=True,num_workers=loaders)

    dummy_input = iter(dl).next()
    n_classes = len(ds.points[ds.y].unique())
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = architecture(n_classes,dummy_input[0].shape).to(device)
    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate, momentum=momentum)

    # Export the untrained network in Open Neural Network Exchange format
    onnx.export(model,dummy_input[0].cuda(),'hydranet.onnx',verbose=False)

    from learning import train


    train(model,optimizer,criterion,dl,device,epochs)
