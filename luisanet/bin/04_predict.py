import os
import joblib
import rasterio as rio
from osgeo import gdal
import torch
import itertools
import numpy as np
import rasterio
from rasterio.windows import Window
from glob import glob
from tqdm import tqdm

from ..architectures import Net
from ..config import (
        project_name,
        dir_storage_slow,
        dir_storage_fast,
        dir_preprocessing,
        dir_deep_learning,
        path_points_source,
        path_nl,
        dir_landsat,
        dir_ahn,
        path_points_preprocessed,
        dir_blocks,
)

if __name__ == '__main__':

    #Paths of test datasets
    path_out = '/mnt/hydra/hydranet/test.tif'
    path_elevation = '/mnt/gaia/tmp/hydranet/data/test/wagga.tif'


    class SingleSourceDataset(torch.utils.data.Dataset):
        def __init__(self,path_tif, padding):
            self.path_tif = path_tif
            self.padding = padding
            with rio.open(path_tif) as src:
                self.bounds = src.bounds
                self.width = src.width
                self.height = src.height

                self.min_x = self.bounds[0]
                self.max_x = self.bounds[2]
                self.min_y = self.bounds[3]
                self.max_y = self.bounds[1]

                self.top_left = src.index(self.min_x,self.min_y)


                self.x_size = (self.max_x - self.min_x) // self.width
                self.y_size = (self.max_y - self.min_y) // self.height

                self.x_range = np.arange(self.min_x,self.max_x,self.x_size)
                self.y_range = np.arange(self.min_y,self.max_y,self.y_size)
                self.xy = list(itertools.product(self.x_range,self.y_range))
                self.indices = list(range(len(self.xy)))

            self.length = self.height*self.width

        def __len__(self):
            return self.length

        def __getitem__(self,idx):
            buffer = (self.padding*2)+1

            with rio.open(self.path_tif) as src:
                # Note - index takes coordinates in X,Y order
                row_idx, col_idx = self.xy[idx]
                row, col = src.index(row_idx,col_idx)
                central_pixel = src.read(window=rio.windows.Window(row,col,1,1))
                row -= self.padding
                col -= self.padding

                buffer = (self.padding*2)+1
                window = rio.windows.Window(row,col,buffer,buffer)
                arr = src.read(1,window=window)
                row_missing = buffer - arr.shape[0]
                col_missing = buffer - arr.shape[1]
                pad_width = np.max([row_missing,col_missing])

                log = f'\n{arr.shape}\nrow_missing:{row_missing}\ncol_missing: {col_missing}\npad_width: {pad_width}'

                if arr.shape != (buffer,buffer):
                    old_shape = arr.shape

                    arr = np.pad(arr,pad_width=pad_width,mode='reflect')
                    log += f'\npadded to {arr.shape}'
                    if row_missing > 0:
                        log += '\nrow_missing > 0'
                        if row < self.padding+1:
                            log += '\nrow < self.padding+1'
                            arr = arr[0:buffer,:]

                        if row > self.padding:
                            log += '\n row > self.padding+1'
                            arr = arr[-buffer:,:]
                        else:
                            log += f'\nrow:{row}'
                    else:
                        log += '\n row_missing not > 0'
                        arr = arr[pad_width:pad_width+buffer,:]

                    if col_missing > 0:
                        log += '\ncol_missing > 0'
                        if col < self.padding+1:
                            log += '\n col < self.padding+1'
                            arr = arr[:,0:buffer]
                        if col > self.padding:
                            log += '\n col > self.padding+1'
                            arr = arr[:,-buffer:]
                    else:
                        log += '\n col_missing < 1'
                        log += f'\n array shape: {arr.shape}'
                        arr = arr[:,pad_width:pad_width+buffer]
                        log += f'\ncut to {arr.shape} with {pad_width}:{pad_width+buffer}'
                arr = np.expand_dims(arr,0)
                assert arr.shape == (1,buffer,buffer), f'ERROR:{old_shape} {arr.shape} {row_missing} {col_missing} {log}'
            return(torch.from_numpy(arr.astype(np.float32)))


    n_classes = 5


    ds = SingleSourceDataset(path_elevation,padding=10)
    batch_size = 16000
    dl = torch.utils.data.DataLoader(ds,batch_size=batch_size,shuffle=False,num_workers=15)

    window_size = 21
    model = Net(n_classes,[128,1,window_size,window_size])
    model.load_state_dict(torch.load('./hydranet_tmp.pth'))
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    with rio.open(ds.path_tif) as src:
        with rio.Env():
            profile = src.profile
            profile.update(count=n_classes,dtype=rio.float32)

        with rio.open(path_out,'w',**profile) as dst:
            arr_pred = np.zeros([ds.height,ds.width,n_classes])
            with tqdm(total=len(dl)) as progress_bar:
                for i, batch in enumerate(dl):
                    preds = model(batch).detach().numpy()
                    progress_bar.update(1) # update progress
                    indices = range(i*batch_size,(i+1)*batch_size)
                    for i in range(len(indices)):
                        idx = indices[i]
                        x_ref, y_ref  = ds.xy[idx]
                        x, y = src.index(x_ref,y_ref)
                        arr_pred[y,x,:] = preds[i,:]
            print(arr_pred.shape)
            arr_pred = np.moveaxis(arr_pred, 2, 0)
            dst.write(arr_pred.astype(np.float32))
