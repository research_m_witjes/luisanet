#Import libraries
import geopandas as gpd
import joblib
import matplotlib.pyplot as plt
import numpy as np
import torchvision as tv
import torch
import torch.nn as nn
from tqdm import tqdm
import netron
import torch.onnx as onnx
from torch.utils.tensorboard import SummaryWriter
import os
import shutil
import sacred
import multiprocessing as mp

# Import custom libraries
from ..dataset import LandsatDataset, MultiDataset
from ..learning import train
from ..architectures import Net, Hydra
from .. import transformations as trfm
from ..config import (
        project_name,
        dir_storage_slow,
        dir_storage_fast,
        dir_preprocessing,
        dir_deep_learning,
        path_points_source,
        path_nl,
        dir_landsat,
        dir_ahn,
        path_points_preprocessed,
        dir_blocks,
)

if __name__ == '__main__':

    points = joblib.load(path_points_preprocessed).to_crs(epsg=28992)

    columns = []
    for c in points.columns:
        if 'landsat' in c:
            columns.append(c)
    print(len(columns))

    bs = 16
    lr = 0.001
    batch_size = 128
    epochs = 300
    criterion = nn.CrossEntropyLoss()
    momentum = 0.9
    architecture = Hydra
    loaders = 40

    ds = MultiDataset(
        path_points=path_points_preprocessed,
        y='clc_1',
        point_columns_x=columns,
        window=10,
        elevation=True,
        transform=tv.transforms.Compose([trfm.ToTensor()]),
        dir_blocks=dir_blocks)

    dl = torch.utils.data.DataLoader(ds,batch_size=bs,shuffle=True,num_workers=np.min([bs,mp.cpu_count()]))

    dummy_input = iter(dl).next()
    for i in dummy_input:
        print(type(i))
        if type(i) is list:
            for j in i:
                print('\t',type(j),j.shape)
        else:
            print(i.shape)

    n_classes = len(ds.points[ds.y].unique())
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # n_classes,input_shape_5m=None,input_shape_30m=None, n_conv=5,n_full=1
    dsm = dummy_input[0][0]
    col = dummy_input[0][1]

    print("DSM shape:",dsm.shape)
    print("COL shape:",col.shape)
    model = architecture(n_classes,dsm.shape,col.shape).to(device)


    model_name = 'hydranet_dsm_pointdata'
    train(model,criterion,dl,device,epochs,model_name,f'./{model_name}_tmp.pth')
