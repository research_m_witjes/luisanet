from osgeo import gdal
import torch
import geopandas as gpd
import rasterio as rio
import numpy as np
import importlib
from pathlib import Path
from tqdm import tqdm
import os
import os.path as osp
import argparse
from glob import glob

from .. import architectures, sampling, legends, config, util, learning

dir_repo = str(Path('file').parent.parent.parent)
dir_repo = '/mnt/hydra/luisanet'

if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('--path_weights',default=None)
    a = p.parse_args()
    
    c = config.luisanet_lvl2_nl_landsat_hydra
    
    if a.path_weights is None:
        path_weights = learning.get_best_model(c['model_name'])
    else:
        path_weights = a.path_weights
    loss = osp.split(path_weights)[1].split("_")[5]
    dir_out = osp.join(dir_repo,'predictions',c['model_name'],loss)
    print(dir_out)
    os.makedirs(dir_out,exist_ok=True)
    batch_size = 1000

    tiles = gpd.read_file(c['path_tiles']).geometry.values
    print(len(tiles))

    input_urls = c['input_urls']
    legend_key = c['legend_key']
    legend_key_reverse = np.array([*legend_key.keys()])

    nodata = 0

    n_channels = len(input_urls)
    n_classes = len(set(legend_key.values()))

    # device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device = torch.device('cpu')
    model = architectures.UNet(n_channels, n_classes,downsample=c['downsample']).to(device)
    state_dict = torch.load(path_weights)
    model.load_state_dict(state_dict)

    print(dir_out)


    def predict_tile(i, geom,dir_out):
        smp = sampling.WindowSampler(input_urls, geom.bounds)
    
        path_probabilities = osp.join(dir_out, f"tile{i}_p.tif")
        path_hard_class = osp.join(dir_out, f"tile{i}_class.tif")

        batcher = smp.batchgen(
            moving=False,
            augment=False,
            return_indices=True,
            batch_size=batch_size,
        )

        out = np.full((n_classes, smp.h, smp.w), nodata,dtype=np.float32)
        out_class = np.full((smp.h, smp.w), nodata)

        for indices, x, __ in batcher:
            batch = torch.from_numpy(x.astype(np.float32)).to(device)
            pred = model(batch).detach().numpy()
            pred_class = legend_key_reverse[pred.argmax(axis=1)]

            for (i, j), p, p_class in zip(indices, pred, pred_class):
                _win = out_class[
                    i:i+p.shape[1],
                    j:j+p.shape[2],
                ]
                di, dj = _win.shape
                out[:,i:i+di,j:j+dj] = p[:,:di,:dj]
                out_class[i:i+di,j:j+dj] = p_class[:di,:dj]
        with rio.open(
            path_probabilities, 'w',
            **{**smp.profile, 'count': n_classes, 'dtype': np.float32},
        ) as dst:
            dst.write(out)

        with rio.open(
            path_hard_class, 'w',
            **{**smp.profile, 'count': 1, 'dtype': np.uint16},
        ) as dst:
            dst.write(out_class.astype(np.uint16), 1)

    for i, geom in tqdm(enumerate(tiles),total=tiles.size,desc='writing tiles'):
        predict_tile(i, geom,dir_out)

    # Aggregate predictions to a single cloud-optimized geotiff

    # tiles to vrt

paths_tiles_class = glob(f"{dir_out}*tile_*_class.tif")
paths_tiles_probs = glob(f"{dir_out}*tile_*_p.tif")
path_vrt = 'temp.vrt'  # path to vrt to build
gdal.BuildVRT(path_vrt, paths_tiles_class)

    # vrt to cog