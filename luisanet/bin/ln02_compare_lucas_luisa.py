import argparse
import pandas as pd
import numpy as np
from ..legends import lucas_1, luisa_1



if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument("--path_crosstab",default='/mnt/hydra/luisanet/lucas1_luisa1_crosstab.csv')
    p.add_argument("--path_weights",default='/mnt/hydra/luisanet/lucas1_luisa1_weights.csv')
    a = p.parse_args()
    
    with open(a.path_weights) as f:
        ncols = len(f.readline().split(','))
    wt = np.loadtxt(a.path_weights, delimiter=',', skiprows=1, usecols=range(1,ncols))
    ct = np.loadtxt(a.path_crosstab, delimiter=',', skiprows=1,usecols=range(1,ncols))

    wt_good = wt.copy()
    wt_bad = wt.copy()
    wt_good[wt_good < 0] = 0
    wt_bad[wt_bad >0] = 0
    wt_bad = np.abs(wt_bad)
    
    print('pre\trec\tf1')
    for i in range(len(lucas_1)):
        lucas_class = list(lucas_1.values())[i]
        good = ct*wt_good
        bad = ct*wt_bad
        total = int(np.sum(ct[i]))
        positives = np.sum(np.abs(ct[i]))
        negatives = total - positives
        true_positives = 0
        false_positives = 0
        false_negatives = 0
        correct = np.sum(good[i])
        for j in range(ct.shape[1]):
            true_positives_luisa_j = int((good[i,j]))
            false_positives_luisa_j = int((bad[i,j]))
            true_positives += true_positives_luisa_j
            false_positives += false_positives_luisa_j
            false_negatives += (np.sum(bad[:,j])-false_positives_luisa_j)
        
        precision = true_positives / (true_positives + false_positives)
        recall = true_positives / (true_positives + false_negatives)
        f1 = 2 * ((precision * recall) / (precision + recall))
        print(f"{precision:.2f}\t{recall:.2f}\t{f1:.2f}\t{lucas_class}")

        # TODO: Calculate macro averages
        # TODO: Verify whether recall and f1 are calculated correctly
        # TODO: Export results in csv



