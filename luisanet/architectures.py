from torchvision import models
import torch
import torch.nn as nn
import torch.nn.functional as F

class Net(nn.Module):
    def __init__(self,n_classes,input_shape,n_conv=5,n_full=1):
        super(Net, self).__init__()
        self.n_conv = n_conv
        self.batch_size = input_shape[0]
        self.n_channels = input_shape[1]
        self.x = input_shape[2]
        self.y = input_shape[3]
        self.conv = nn.Conv2d(
            in_channels=self.n_channels, 
            out_channels=self.n_channels, 
            kernel_size=3)
        self.full_shape = (self.x-(n_conv*2)) * (self.y-(n_conv*2)) * self.n_channels
        self.n_full = n_full
        self.full_1 = nn.Linear(self.full_shape,128)
        self.full_2 = nn.Linear(128,128)
        self.logit = nn.Linear(128,n_classes)
        print(input_shape)
        
    def forward(self,x):
        for i in range(self.n_conv):
            x = self.conv(x)
            x = nn.functional.relu(x)

        x = x.flatten(start_dim=1)
        
        x = self.full_1(x)
        x = nn.functional.relu(x)

        for i in range(self.n_full-1):
            x = self.full_2(x)
            x = nn.functional.relu(x)
        
        logits = self.logit(x)
        out = nn.functional.softmax(logits,dim=1)
        return out


class Hydra(nn.Module):
    def __init__(self,n_classes,input_shape_5m=None,input_shape_30m=None, n_conv=5,n_full=1):
        super(Hydra, self).__init__()
        self.n_conv = n_conv
        self.batch_size = input_shape_5m[0]
        self.n_channels = input_shape_5m[1]
        self.x = input_shape_5m[2]
        self.y = input_shape_5m[3]
        self.conv = nn.Conv2d(
            in_channels=self.n_channels, 
            out_channels=self.n_channels, 
            kernel_size=3)
        self.full_shape = (self.x-(n_conv*2)) * (self.y-(n_conv*2)) * self.n_channels
        self.n_full = n_full
        self.full_5m = nn.Linear(self.full_shape,128)
        self.full_30m = nn.Linear(input_shape_30m[1],128)
        self.full_cat = nn.Linear(128*2,128)
        self.full = nn.Linear(128,128)
        self.logit = nn.Linear(128,n_classes)
        print(input_shape_5m,input_shape_30m)

    def forward(self,x):

        x_5m, x_30m = x
        for i in range(self.n_conv):
            x_5m = self.conv(x_5m)
            x_5m = nn.functional.relu(x_5m)

        x_5m = x_5m.flatten(start_dim=1)
        
        x_5m = self.full_5m(x_5m)
        x_5m = nn.functional.relu(x_5m)

        x_30m = self.full_30m(x_30m)
        x_30m = nn.functional.relu(x_30m)

        x = tuple([x_5m,x_30m])

        x = torch.cat(x,dim=1)        
        
        x = self.full_cat(x)
        x = nn.functional.relu(x)

        x = self.full(x)
        x = nn.functional.relu(x)

        logits = self.logit(x)
        out = nn.functional.softmax(logits,dim=1)
        return out




class DoubleConv(nn.Module):
    """(convolution => [BN] => ReLU) * 2"""

    def __init__(self, in_channels, out_channels, mid_channels=None):
        super().__init__()
        if not mid_channels:
            mid_channels = out_channels
        
        self.double_conv = nn.Sequential(
            nn.Conv2d(in_channels, mid_channels, kernel_size=3, padding=1, bias=False),
            nn.BatchNorm2d(mid_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid_channels, out_channels, kernel_size=3, padding=1, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        return self.double_conv(x)


class Down(nn.Module):
    """Downscaling with maxpool then double conv"""

    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.maxpool_conv = nn.Sequential(
            nn.MaxPool2d(2),
            DoubleConv(in_channels, out_channels)
        )

    def forward(self, x):
        return self.maxpool_conv(x)


class Up(nn.Module):
    """Upscaling then double conv"""

    def __init__(self, in_channels, out_channels, bilinear=True):
        super().__init__()

        # if bilinear, use the normal convolutions to reduce the number of channels
        if bilinear:
            self.up = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
            self.conv = DoubleConv(in_channels, out_channels, in_channels // 2)
        else:
            self.up = nn.ConvTranspose2d(in_channels, in_channels // 2, kernel_size=2, stride=2)
            self.conv = DoubleConv(in_channels, out_channels)

    def forward(self, x1, x2):
        x1 = self.up(x1)
        # input is CHW
        diffY = x2.size()[2] - x1.size()[2]
        diffX = x2.size()[3] - x1.size()[3]

        x1 = F.pad(x1, [diffX // 2, diffX - diffX // 2,
                        diffY // 2, diffY - diffY // 2])
        # if you have padding issues, see
        # https://github.com/HaiyongJiang/U-Net-Pytorch-Unstructured-Buggy/commit/0e854509c2cea854e247a9c615f175f76fbb2e3a
        # https://github.com/xiaopeng-liao/Pytorch-UNet/commit/8ebac70e633bac59fc22bb5195e513d5832fb3bd
        x = torch.cat([x2, x1], dim=1)
        return self.conv(x)


class OutConv(nn.Module):
    def __init__(self, in_channels, out_channels,kernel_size=1):
        super(OutConv, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=kernel_size)

    def forward(self, x):
        return self.conv(x)

class UNet(nn.Module):
    def __init__(self, n_channels, n_classes, bilinear=False,downsample=118):
        super(UNet, self).__init__()
        self.downsample = downsample
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.bilinear = bilinear

        self.inc = DoubleConv(n_channels, 64)
        self.down1 = Down(64, 128)
        self.down2 = Down(128, 256)
        self.down3 = Down(256, 512)
        factor = 2 if bilinear else 1
        self.down4 = Down(512, 1024 // factor)
        self.up1 = Up(1024, 512 // factor, bilinear)
        self.up2 = Up(512, 256 // factor, bilinear)
        self.up3 = Up(256, 128 // factor, bilinear)
        self.up4 = Up(128, 64, bilinear)
        self.outc = OutConv(64, n_classes,kernel_size=self.downsample)

    def forward(self, x):
        x1 = self.inc(x)
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)
        x = self.up1(x5, x4)
        x = self.up2(x, x3)
        x = self.up3(x, x2)
        x = self.up4(x, x1)
        logits = self.outc(x)
        return logits