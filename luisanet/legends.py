import numpy as np
from . import util


def invert_dict(d):
    di = {value:[] for value in d.values()}
    for key in d:
        di[d[key]].append(key)
    return di


lucas_to_s2glc = {
    'A11':'Artificial Surfaces', 'A12':'Artificial Surfaces', 'A13':'Artificial Surfaces',
    'A21':'Artificial Surfaces', 'A22':'Artificial Surfaces', 'A30':'Artificial Surfaces',
    'B11':'Cultivated Areas', 'B12':'Cultivated Areas', 'B13':'Cultivated Areas',
    'B14':'Cultivated Areas', 'B15':'Cultivated Areas', 'B16':'Cultivated Areas',
    'B17':'Cultivated Areas', 'B18':'Cultivated Areas', 'B19':'Cultivated Areas',
    'B21':'Cultivated Areas', 'B22':'Cultivated Areas', 'B23':'Cultivated Areas',
    'B31':'Cultivated Areas', 'B32':'Cultivated Areas', 'B33':'Cultivated Areas',
    'B34':'Cultivated Areas', 'B35':'Cultivated Areas', 'B36':'Cultivated Areas',
    'B37':'Cultivated Areas', 'B41':'Cultivated Areas', 'B42':'Cultivated Areas',
    'B43':'Cultivated Areas', 'B44':'Cultivated Areas', 'B45':'Cultivated Areas',
    'B51':'Cultivated Areas', 'B52':'Cultivated Areas', 'B53':'Cultivated Areas',
    'B54':'Cultivated Areas', 'B55':'Cultivated Areas', 'B71':'Broadleaf Tree Cover',
    'B72':'Broadleaf Tree Cover', 'B73':'Broadleaf Tree Cover', 'B74':'Broadleaf Tree Cover',
    'B75':'?',
    'B76':'Broadleaf Tree Cover', 'B77':'Broadleaf Tree Cover',
    'B81':'Broadleaf Tree Cover', 'B82':'Vineyards', 'B83':'Broadleaf Tree Cover',
    'B84':'?',
    'C10':'Broadleaf Tree Cover', 'C21':'Coniferous Tree Cover',
    'C22':'Coniferous Tree Cover', 'C23':'Coniferous Tree Cover',
    'C31':'?',
    'C32':'?',
    'C33':'?',
    'D10':'Moors and Heathland', 'D20':'Moors and Heathland',
    'E10':'Herbaceous Vegetation',
    'E20':'Herbaceous Vegetation', 'E30':'Herbaceous Vegetation',
    'F10':'Natural Material Surfaces', 'F20':'Natural Material Surfaces',
    'F30':'?',
    'F40':'Natural Material Surfaces', 'G11':'Water Bodies', 'G12':'Water Bodies',
    'G21':'Water Bodies', 'G22':'Water Bodies', 'G30':'Water Bodies', 'G40':'Water Bodies',
    'G50':'Permanent Snow Cover', 'H11':'Marshes', 'H12':'Peatbogs', 'H21':'Marshes',
    'H22':'Marshes',
    'H23':'Marshes'}

s2glc_to_lucas = invert_dict(lucas_to_s2glc)

lucas_classes = [
    'A11', 'A12', 'A13', 'A21', 'A22', 'A30', 'B11', 'B12', 'B13', 'B14', 'B15', 'B16',
    'B17', 'B18', 'B19', 'B21', 'B22', 'B23', 'B31', 'B32', 'B33', 'B34', 'B35', 'B36',
    'B37', 'B41', 'B42', 'B43', 'B44', 'B45', 'B51', 'B52', 'B53', 'B54', 'B55', 'B71',
    'B72', 'B73', 'B74', 'B75', 'B76', 'B77', 'B81', 'B82', 'B83', 'B84', 'C10', 'C21',
    'C22', 'C23', 'C31', 'C32', 'C33', 'D10', 'D20', 'E10', 'E20', 'E30', 'F10', 'F20',
    'F30', 'F40', 'G11', 'G12', 'G21', 'G22', 'G30', 'G40', 'G50', 'H11', 'H12', 'H21',
    'H22', 'H23']


lucas_to_s2glc = {
    'A11':'Artificial Surfaces', 'A12':'Artificial Surfaces', 'A13':'Artificial Surfaces',
    'A21':'Artificial Surfaces', 'A22':'Artificial Surfaces', 'A30':'Artificial Surfaces',
    'B11':'Cultivated Areas', 'B12':'Cultivated Areas', 'B13':'Cultivated Areas',
    'B14':'Cultivated Areas', 'B15':'Cultivated Areas', 'B16':'Cultivated Areas',
    'B17':'Cultivated Areas', 'B18':'Cultivated Areas', 'B19':'Cultivated Areas',
    'B21':'Cultivated Areas', 'B22':'Cultivated Areas', 'B23':'Cultivated Areas',
    'B31':'Cultivated Areas', 'B32':'Cultivated Areas', 'B33':'Cultivated Areas',
    'B34':'Cultivated Areas', 'B35':'Cultivated Areas', 'B36':'Cultivated Areas',
    'B37':'Cultivated Areas', 'B41':'Cultivated Areas', 'B42':'Cultivated Areas',
    'B43':'Cultivated Areas', 'B44':'Cultivated Areas', 'B45':'Cultivated Areas',
    'B51':'Cultivated Areas', 'B52':'Cultivated Areas', 'B53':'Cultivated Areas',
    'B54':'Cultivated Areas', 'B55':'Cultivated Areas', 'B71':'Broadleaf Tree Cover',
    'B72':'Broadleaf Tree Cover', 'B73':'Broadleaf Tree Cover', 'B74':'Broadleaf Tree Cover',
    'B75':'?',
    'B76':'Broadleaf Tree Cover', 'B77':'Broadleaf Tree Cover',
    'B81':'Broadleaf Tree Cover', 'B82':'Vineyards', 'B83':'Broadleaf Tree Cover',
    'B84':'?',
    'C10':'Broadleaf Tree Cover', 'C21':'Coniferous Tree Cover',
    'C22':'Coniferous Tree Cover', 'C23':'Coniferous Tree Cover',
    'C31':'?',
    'C32':'?',
    'C33':'?',
    'D10':'Moors and Heathland', 'D20':'Moors and Heathland',
    'E10':'Herbaceous Vegetation',
    'E20':'Herbaceous Vegetation', 'E30':'Herbaceous Vegetation',
    'F10':'Natural Material Surfaces', 'F20':'Natural Material Surfaces',
    'F30':'?',
    'F40':'Natural Material Surfaces', 'G11':'Water Bodies', 'G12':'Water Bodies',
    'G21':'Water Bodies', 'G22':'Water Bodies', 'G30':'Water Bodies', 'G40':'Water Bodies',
    'G50':'Permanent Snow Cover', 'H11':'Marshes', 'H12':'Peatbogs', 'H21':'Marshes',
    'H22':'Marshes',
    'H23':'Marshes'}; s2glc_to_lucas = invert_dict(lucas_to_s2glc)

lucas_to_label = {
    'A11':'Buildings with 1 to 3 floors',
    'A12':'Buildings with more than 3 floors',
    'A13':'Greenhouses',
    'A21':'Non built-up area features',
    'A22':'Non built-up linear features',
    'A30':'Other artificial areas',
    'B11':'Common wheat',
    'B12':'Durum wheat',
    'B13':'Barley',
    'B14':'Rye',
    'B15':'Oats',
    'B16':'Maize',
    'B17':'Rice',
    'B18':'Triticale',
    'B19':'Other cereals',
    'B21':'Potatoes',
    'B22':'Sugar beet',
    'B23':'Other root crops',
    'B31':'Sunflower',
    'B32':'Rape and turnip rape',
    'B33':'Soya',
    'B34':'Cotton',
    'B35':'Other fibre and oleaginous crops',
    'B36':'Tobacco',
    'B37':'Other non-permanent industrial crops',
    'B41':'Dry pulses',
    'B42':'Tomatoes',
    'B43':'Other fresh vegetables',
    'B44':'Floriculture and ornamental plants',
    'B45':'Strawberries',
    'B51':'Clovers',
    'B52':'Lucerne',
    'B53':'Other leguminous and mixtures for fodder',
    'B54':'Mixed cereals for fodder',
    'B55':'Temporary grasslands',
    'B71':'Apple fruit',
    'B72':'Pear fruit',
    'B73':'Cherry fruit',
    'B74':'Nuts trees',
    'B75':'Other fruit trees and berries',
    'B76':'Oranges',
    'B77':'Other citrus fruit',
    'B81':'Olive groves',
    'B82':'Vineyards',
    'B83':'Nurseries',
    'B84':'Permanent industrial crops',
    'BX1':'Arable land (only PI)',
    'BX2':'Permanent crops (only PI)',
    'C10':'Broadleaved woodland',
    'C20':'Coniferous woodland',
    'C21':'Spruce dominated coniferous woodland',
    'C22':'Pine dominated coniferous woodland',
    'C23':'Other coniferous woodland',
    'C30':'Mixed woodland',
    'C31':'Spruce dominated mixed woodland',
    'C32':'Pine dominated mixed woodland',
    'C33':'Other mixed woodland',
    'D10':'Shrubland with sparse tree cover',
    'D20':'Shrubland without tree cover',
    'E10':'Grassland with sparse tree/shrub cover',
    'E20':'Grassland without tree/shrub cover',
    'E30':'Spontaneously vegetated surfaces',
    'F00':'Bare land',
    'F10':'Rocks and stones',
    'F20':'Sand',
    'F30':'Lichens and moss',
    'F40':'Other bare soil',
    'G10':'Inland water bodies',
    'G11':'Inland fresh water bodies',
    'G12':'Inland salty water bodies',
    'G20':'Inland running water',
    'G21':'Inland fresh running water',
    'G22':'Inland salty running water',
    'G30':'Transitional water bodies',
    'G40':'Marine sea',
    'G50':'Glaciers, permanent snow',
    'H11':'Inland marshes',
    'H12':'Peatbogs',
    'H21':'Salt marshes',
    'H22':'Salines and other chemical deposits',
    'H23':'Intertidal flats',
    'C11':'Broadleaved and evergreen woodland - Boreal forest',
    'C12':'Broadleaved and evergreen woodland - Hemiboreal forest and nemoral coniferous forest and mixed broadleaved-coniferous forest',
    'C13':'Broadleaved and evergreen woodland - Alpine coniferous forest',
    'C14':'Broadleaved and evergreen woodland - Acidophilous oak and oak-birch forest',
    'C15':'Broadleaved and evergreen woodland - Mesophytic deciduous forest',
    'C16':'Broadleaved and evergreen woodland - Beech forest',
    'C17':'Broadleaved and evergreen woodland - Mountainous beech forest',
    'C18':'Broadleaved and evergreen woodland - Thermophilous deciduous forest',
    'C19':'Broadleaved and evergreen woodland - Broadleaved evergreen forest',
    'C1A':'Broadleaved and evergreen woodland - Coniferous forest of the Mediterranean region',
    'C1B':'Broadleaved and evergreen woodland - Mire and swamp forests',
    'C1C':'Broadleaved and evergreen woodland - Floodplain forest',
    'C1D':'Broadleaved and evergreen woodland - Non-riverine alder, birch or aspen forest',
    'C1E':'Broadleaved and evergreen woodland - Plantations and self-sown exotic forest',
    'C21':'Coniferous woodland - Boreal forest'}

luisa_4 = {
    '1111' : '1111 - High density urban fabric',
    '1121' : '1121 - Medium density urban fabric',
    '1122' : '1122 - Low density urban fabric',
    '1123' : '1123 - Isolated or very low density urban fabric',
    '1130' : '1130 - Urban vegetation',
    '1210' : '1210 - Industrial or commercial units',
    '1221' : '1221 - Road and rail networks and associated land',
    '1222' : '1222 - Major stations',
    '1230' : '1230 - Port areas',
    '1241' : '1241 - Airport areas',
    '1242' : '1242 - Airport terminals',
    '1310' : '1310 - Mineral extraction sites',
    '1320' : '1320 - Dump sites',
    '1330' : '1330 - Construction sites',
    '1410' : '1410 - Green urban areas',
    '1421' : '1421 - Sport and leisure green',
    '1422' : '1422 - Sport and leisure built-up',
    '2110' : '2110 - Non irrigated arable land',
    '2120' : '2120 - Permanently irrigated land',
    '2130' : '2130 - Rice fields',
    '2210' : '2210 - Vineyards',
    '2220' : '2220 - Fruit trees and berry plantations',
    '2230' : '2230 - Olive groves',
    '2310' : '2310 - Pastures',
    '2410' : '2410 - Annual crops associated with permanent crops',
    '2420' : '2420 - Complex cultivation patterns',
    '2430' : '2430 - Land principally occupied by agriculture',
    '2440' : '2440 - Agro-forestry areas',
    '3110' : '3110 - Broad-leaved forest',
    '3120' : '3120 - Coniferous forest',
    '3130' : '3130 - Mixed forest',
    '3210' : '3210 - Natural grassland',
    '3220' : '3220 - Moors and heathland',
    '3230' : '3230 - Sclerophyllous vegetation',
    '3240' : '3240 - Transitional woodland shrub',
    '3310' : '3310 - Beaches, dunes and sand plains',
    '3320' : '3320 - Bare rock',
    '3330' : '3330 - Sparsely vegetated areas',
    '3340' : '3340 - Burnt areas',
    '3350' : '3350 - Glaciers and perpetual snow',
    '4000' : '4000 - Wetlands',
    '5110' : '5110 - Water courses',
    '5120' : '5120 - Water bodies',
    '5210' : '5210 - Coastal lagoons',
    '5220' : '5220 - Estuaries',
    '5230' : '5230 - Sea and ocean'}

luisa_2 = {
    '11' : '11 - Urban fabric',
    '12' : '12 - Industrial, commercial and transport units',
    '13' : '13 - Mine, dump and construction sites',
    '14' : '14 - Artificial, non-agricultural vegetated areas',
    '21' : '21 - Arable land',
    '22' : '22 - Permanent crops',
    '23' : '23 - Pastures',
    '24' : '24 - Heterogeneous agricultural areas',
    '31' : '31 - Forest and seminatural areas',
    '32' : '32 - Shrub and/or herbaceous vegetation associations',
    '33' : '33 - Open spaces with little or no vegetation',
    '40' : '40 - Wetlands',
    '51' : '51 - Inland waters',
    '52' : '52 - Marine waters'
}

lucas_3 = {
    'A11' : 'A11 - Buildings with one to three floors',
    'A12' : 'A12 - Buildings with more than three floors',
    'A13' : 'A13 - Greenhouses',
    'A21' : 'A21 - Non built-up area features',
    'A22' : 'A22 - Non built-up linear features',
    'A30' : 'A30 - Other artificial areas',
    'B11' : 'B11 - Common wheat',
    'B12' : 'B12 - Durum wheat',
    'B13' : 'B13 - Barley',
    'B14' : 'B14 - Rye',
    'B15' : 'B15 - Oats',
    'B16' : 'B16 - Maize',
    'B17' : 'B17 - Rice',
    'B18' : 'B18 - Triticale',
    'B19' : 'B19 - Other cereals',
    'B21' : 'B21 - Potatoes',
    'B22' : 'B22 - Sugar beet',
    'B23' : 'B23 - Other root crops',
    'B31' : 'B31 - Sunflower',
    'B32' : 'B32 - Rape and turnip rape',
    'B33' : 'B33 - Soya',
    'B34' : 'B34 - Cotton',
    'B35' : 'B35 - Other fibre and olaginous crops',
    'B36' : 'B36 - Tobacco',
    'B37' : 'B37 - Other non-permanent industrial crops',
    'B41' : 'B41 - Dry pulses',
    'B42' : 'B42 - Tomatoes',
    'B43' : 'B43 - Other fresh vegetables',
    'B44' : 'B44 - Floriculture and ornamental plants',
    'B45' : 'B45 - Strawberrries',
    'B51' : 'B51 - Clovers',
    'B52' : 'B52 - Lucerne',
    'B53' : 'B53 - Other leguminous and mixtures for fodder',
    'B54' : 'B54 - Mixed cereals for fodder',
    'B55' : 'B55 - Temporary grasslands',
    'B71' : 'B71 - Apple fruit',
    'B72' : 'B72 - Pear fruit',
    'B73' : 'B73 - Cherry fruit',
    'B74' : 'B74 - Nuts trees',
    'B75' : 'B75 - Other fruit trees and berries',
    'B76' : 'B76 - Oranges',
    'B77' : 'B77 - Other citrus fruit',
    'B81' : 'B81 - Olive groves',
    'B82' : 'B82 - Vineyards',
    'B83' : 'B83 - Nurseries',
    'B84' : 'B84 - Permanent industrial crops',
    'C10' : 'C10 - Broadleaved woodland',
    'C21' : 'C21 - Spruce dominated coniferous woodland',
    'C22' : 'C22 - Pine dominated coniferous woodland',
    'C23' : 'C23 - Other coniferous woodland',
    'C31' : 'C31 - Spruce dominated mixed woodland',
    'C32' : 'C32 - Pine dominated mixed woodland',
    'C33' : 'C33 - Other mixed woodland',
    'D10' : 'D10 - Shrubland with sparse tree cover',
    'D20' : 'D20 - Shrubland without tree cover',
    'E10' : 'E10 - Grassland with sparse tree/shrub cover',
    'E20' : 'E20 - Grassland without tree/shrub cover',
    'E30' : 'E30 - Spontaneously re-vegetated surfaces',
    'F10' : 'F10 - Rocks and stones',
    'F20' : 'F20 - Sand',
    'F30' : 'F30 - Lichens and moss',
    'F40' : 'F40 - Other bare soil',
    'G11' : 'G11 - Inland fresh water bodies',
    'G12' : 'G12 - Inland salty water bodies',
    'G21' : 'G21 - Inland fresh running water',
    'G22' : 'G22 - Inland salty running water',
    'G30' : 'G30 - Transitional water bodies',
    'G40' : 'G40 - Sea and ocean',
    'G50' : 'G50 - Glaciers, permanent snow',
    'H11' : 'H11 - Inland marshes',
    'H12' : 'H12 - Peatbogs',
    'H21' : 'H21 - Salt marshes',
    'H22' : 'H22 - Salines and other chemical deposits',
    'H23' : 'H23 - Intertidal flats'}


luisa_1 = {
    "1" : "1 - Artificial surfaces",
    "2" : "2 - Agricultural areas",
    "3" : "3 - Forest and seminatural areas",
    "4" : "4 - Wetlands",
    "5" : "5 - Water bodies"}

lucas_1 = {
    "A" : "A00 - Artificial land",
    "B" : "B00 - Cropland",
    "C" : "C00 - Woodland",
    "D" : "D00 - Shrubland",
    "E" : "E00 - Grassland",
    "F" : "F00 - Bare land and lichens/moss",
    "G" : "G00 - Water areas",
    "H" : "H00 - Wetlands"}


def aggregate_key(key,divisor):
    ints = [int(i) for i in list(key.keys())]
    ints_aggregated = [int(i/divisor) for i in ints]
    key_aggregated = dict(zip(ints,ints_aggregated))
    return key_aggregated

luisa4_to_luisa1 = aggregate_key(luisa_4,1000)
luisa4_to_luisa2 = aggregate_key(luisa_4,100)
luisa4_to_luisa3 = aggregate_key(luisa_4,10)

# luisa4_to_grasslands = {
#     2310 : 2310,
#     3220 : 3220,
#     3230 : 3230,
#     3240 : 3240,
#     4000 : 4000
# }

luisa4_to_grass1 = {
    # lvl4 classes aggregated to lvl1 (non-grassland)
    1111 : 1, 1121 : 1, 1122 : 1, 1123 : 1, 1130 : 1, 1210 : 1, 1221 : 1,
    1222 : 1, 1230 : 1, 1241 : 1, 1242 : 1, 1310 : 1, 1320 : 1, 1330 : 1,
    1410 : 1, 1421 : 1, 1422 : 1,
    2110 : 2, 2120 : 2, 2130 : 2, 2210 : 2, 2220 : 2, 2230 : 2,
    2410 : 2, 2420 : 2, 2430 : 2, 2440 : 2,
    3110 : 3, 3120 : 3, 3130 : 3, 3310 : 3, 
    3320 : 3, 3330 : 3, 3340 : 3, 3350 : 3,
    5110 : 5, 5120 : 5, 5210 : 5, 5220 : 5, 5230 : 5,
    # relevant luisa lvl4 classes
    2310 : 2310, # pastures
    3210 : 3210, # natural grassland
    3220 : 3220, # moors and heathland
    3230 : 3230, # sclerophyllous vegetation
    3240 : 3240, # Transitional woodland shrub
    4000 : 4000,  # Wetlands
}

def dict_to_idx(dictionary):
    ints = [int(i) for i in list(dictionary.keys())]
    idxs = dict(zip(ints,range(len(ints))))
    return idxs

luisa4_to_idx = dict_to_idx(luisa_4)
luisa2_to_idx = dict_to_idx(luisa_2)
luisa1_to_idx = dict_to_idx(luisa_1)
grass1_to_idx = {
    1 : 0, 2 : 1, 3 : 2, 5 : 3, # aggregated classes
    2310 : 4,
    3210 : 5,
    3220 : 6,
    3230 : 7,
    3240 : 8,
    4000 : 9,
}

from . import config
def reclass(array,key):
    try:
        array = np.vectorize(key.get)(array)
    except:
        single_value = np.unique(array)[1]
        replacement = key[single_value]
        array[:,:] = replacement


    array = array.astype(np.float32)
    return array
