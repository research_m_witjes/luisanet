import torch
import numpy as np

class Rotate(object):
    def __init__(self):
        pass
    def __call__(self,sample):
        block, y = sample
        rotations = torch.randint(low=0,high=3,size=[1])
        block = np.rot90(block,axes=[1,2],k=rotations).copy()
        
        return block, y


class Flip(object):
    def __init__(self):
        pass
    def __call__(self,sample):
        block, y = sample
        if torch.randint(low=0,high=2,size=[1]) == 1:
            block = np.flip(block,axis=2).copy()
            
        return block, y


class Normalize(object):
    def __init__(self):
        pass
    def __call__(self,sample):
        block, y = sample
        normalized = block / np.linalg.norm(block)
        return normalized, y


class ToTensor(object):
    def __call__(self, sample):
        block, y = sample
        if type(block) is list:
            for i in range(len(block)):
                block[i] = torch.from_numpy(block[i])
        else:
            block = torch.from_numpy(block)
        return block, torch.from_numpy(y)
