import rasterio as rio
import os
import numpy as np
from rasterio.features import geometry_window
from tqdm import tqdm

def new_raster(path_old,new_array,path_new,overwrite=True,dtype='uint8',nodata=None):
    bands = new_array.shape[0] if len(new_array.shape) > 2 else 1
    if dtype == 'uint8':
        rio_dtype = rio.uint8
        np_dtype=np.uint8
    elif dtype == 'int16':
        rio_dtype = rio.int16
        np_dtype = np.int16
    elif dtype == 'float32':
        rio_dtype = rio.float32
        np_dtype = np.float32
    new_array = new_array.astype(np_dtype)
    with rio.open(path_old) as src:
        with rio.Env():
            profile=src.profile
            profile.update(count=bands,dtype=rio_dtype)
            if nodata is not None:
                profile.update(nodata=nodata)
    if os.path.exists(path_new) and overwrite:
        os.remove(path_new)
    
    with rio.open(path_new, 'w',**profile) as dst:
        if bands > 2:
            for band in range(bands):
                dst.write(new_array[band,:,:],band+1)
        else:
            dst.write(new_array,1)


def extract_context(pt,path_tif,pad=127):
    with rio.open(path_tif,'r') as src:
        window = geometry_window(src, [pt], pad_x=pad, pad_y=pad, 
                                              north_up=None, rotated=None, pixel_precision=None, boundless=False)
        array = src.read(1,window=window)
        if array.shape[0] % 2 == 0:
            array = array[:-1,:]
            
        if array.shape[1] % 2 == 0:
            array = array[:,:-1]
            
    return array


def overlay(path_raster,gdf,name,verbose=False):
    with rio.open(path_raster) as src:
        coord_list = [(x,y) for x,y in zip(gdf.x , gdf.y)]
        if verbose:
            gdf[name] = [i[0] for i in tqdm(src.sample(coord_list))]
        else:
            gdf[name] = [i[0] for i in src.sample(coord_list)]
    return(gdf)